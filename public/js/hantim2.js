$(function() {
  
  	// vacancy reply form
	$('.replyform-cancel').click(function(){
		$('.replyform').hide();
		$('.replybtn').show();

		return false;
	});

	$('.replybtn').click(function(){
		$('.replyform').toggle();
		if ( $('.replyform').css('display') == 'block' ) $(this).hide(); else $(this).show();

		return false;
	});


	// categories filter popup
	$('.categories_trigger').click(function(e){
		$('.filter-categories').toggle();
		e.stopPropagation(); 
	});	

	$('[class*=filter-categories]').click(function(e) { 
        e.stopPropagation(); 
    });

	$('html').click(function(e){
		$('.filter-categories').hide();
	});


    // companies search
    $('#companyKw').on('keydown', function(e){
        if ( e.which==13 ) $('#frmCompanies').submit();
    });

    // connect vacancy
    $('.connect_company').click(function(){
        $('#company_id').val( $('#company_id').data('company-id') );
        $('#connect_company_container, #companyForm').hide();
        $('#deconnect_company_container').show();
        return false;
    });

    $('.deconnect_company').click(function(){
        $('#company_id').val('');
        $('#connect_company_container, #companyForm').show();
        $('#deconnect_company_container').hide();
        return false;
    });

    $('button.preview').click(function(){
        $('#preview').val('1');
        $('.company_edit form').attr('target', '_blank').submit();
    });

    $('button.submit').click(function(){
        $('#preview').val('');
        $('.company_edit form').removeAttr('target').submit();
    });

	// chosen
	$('.company_edit .chosen').chosen({max_selected_options: 10});

    // index page search
    $('.vacancy_type, .vacancy_category').on('change', function(){

        // type
        var type = [];
        $('.vacancy_type:checked').each(function(){
            type[type.length] = $(this).val();
        });
        searchParams.type = type.join(',');

        // category
        var category = [];
        $('.vacancy_category:checked').each(function(){
            category[category.length] = $(this).val();
        });
        searchParams.category = category.join(',');

        if ( searchParams.category == '' ) {
            $('.categories_trigger').removeClass('hl');
        } else {
            $('.categories_trigger').addClass('hl');
        }

        refreshSearch();
    });

    $('#filter_kw').keyup(function(){
        searchParams.kw = $.trim( $(this).val() );
        refreshSearch();
    });

    $('#filter_location').keyup(function(){
        searchParams.city = $.trim( $(this).val() );
        refreshSearch();
    });

    function refreshSearch() {

        // build url
        var url = '';
        if ( searchParams.type != '' ) url += '&type=' + searchParams.type;
        if ( searchParams.category != '' ) url += '&category=' + searchParams.category;
        if ( searchParams.kw != '' ) url += '&kw=' + searchParams.kw;
        if ( searchParams.city != '' ) url += '&city=' + searchParams.city;
        if ( url != '' ) url = '?' + url;
        url = searchParams.url + url;

        // change history
        history.pushState(searchParams, 'Search', url)

        // load content
        $('#searchResult').load(url);
    }


});