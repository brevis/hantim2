<?php

class UserController extends \BaseController
{

    /**
     * Login action
     *
     * @return Response
     */
    public function login()
    {
        if (Auth::check()) return Redirect::route('home');

        if (Request::isMethod('post')) {

            $userdata = Input::get('form');

            $validator = Validator::make($userdata, User::$loginRules, User::getLoginErrorMessages());
            if ($validator->fails()) {
                return Redirect::route('login')->withInput()->withErrors($validator);
            }

            try {
                $user = User::where('email', '=', $userdata['email'])->firstOrFail();
            } catch (\Exception $e) {
                $user = null;
            }

            if ($user instanceof User) {
                if ($user->enabled) {
                    if (empty($user->confirmation_token)) {
                        if (Auth::attempt($userdata)) {
                            return Redirect::route('home');
                        }
                    } else {
                        return Redirect::to('login')->withInput()->with('msgError', Lang::get('user.account_not_confirmed'));
                    }
                } else {
                    return Redirect::to('login')->withInput()->with('msgError', Lang::get('user.account_not_enabled'));
                }
            }

            return Redirect::to('login')->withInput()->with('msgError', Lang::get('user.email_or_password_wrong'));
        }

        return View::make('user.login');
    }

    /**
     * Logout action
     *
     * @return Response
     */
    public function logout()
    {
        Auth::logout();
        return Redirect::route('home');
    }

    /**
     * Registration action
     *
     * @return Response
     */
    public function register()
    {
        if (Auth::check()) return Redirect::route('home');

        if (Request::isMethod('post')) {
            $validator = Validator::make(Input::get('form'), User::$registrationRules, User::getRegistrationErrorMessages());
            if ($validator->fails()) {
                return Redirect::route('register')->withInput()->withErrors($validator);
            }

            $data = $validator->getData();
            $user = new User;
            $user->email = $data['email'];
            $user->password = Hash::make($data['password']);
            $user->enabled = 1;
            $user->confirmation_token = md5($data['email'] . str_random());
            $user->created_at = time();
            $user->last_ip = Request::server('REMOTE_ADDR');
            $user->save();

            $this->_sendConfirmationMail($user);

            return Redirect::route('register')->with('msgInfo', Lang::get('user.registration_accepted'));
        }

        return View::make('user.register');
    }

    /**
     * Registration confirmation action
     *
     * @return Response
     */
    public function registerConfirm($code)
    {
        if (Auth::check()) return Redirect::route('home');

        try {
            $user = User::where('confirmation_token', '=', $code)->firstOrFail();
        } catch (\Exception $e) {
            $user = null;
        }

        if ($user instanceof User) {
            $user->confirmation_token = '';
            $user->last_ip = Request::server('REMOTE_ADDR');
            $user->save();

            if ($user->enabled) Auth::login($user);
            return Redirect::route('home')->with('msgSuccess', Lang::get('user.confirmation_succes'));
        }

        return Redirect::route('home')->with('msgError', Lang::get('user.confirmation_error'));
    }

    /**
     * Reset password action
     *
     * @return Response
     */
    public function resetPassword()
    {
        if (Auth::check()) return Redirect::route('home');

        if (Request::isMethod('post')) {
            $email = Input::only('email');

            $validator = Validator::make($email, User::$resetPasswordRules, User::getResetPasswordErrorMessages());
            if ($validator->fails()) {
                return Redirect::route('resetpassword')->withInput()->withErrors($validator);
            }

            $response = Password::remind(Input::only('email'), function ($message) {
                $message->subject(Lang::get('user.password_recovery'));
            });

            if ($response == Password::REMINDER_SENT) {
                return Redirect::route('resetpassword')->with('msgInfo', Lang::get('user.reminder_already_sent'));
            }

            return Redirect::route('resetpassword')->withInput()->with('msgError', Lang::get('user.account_not_found'));
        }

        return View::make('user.resetpassword');
    }

    /**
     * Reset password confirmation action
     *
     * @return Response
     */
    public function resetPasswordConfirm($code)
    {
        if (Auth::check()) return Redirect::route('home');

        try {
            $reminder = DB::table('password_reminders')->where('token', $code)->first();
        } catch (\Exception $e) {
            $reminder = null;
        }

        if (!is_object($reminder)) {
            return Redirect::route('home')->with('msgError', Lang::get('user.confirmation_error'));
        }

        if (Request::isMethod('post')) {
            $passwords = array(
                'password' => Input::get('password'),
                'password_confirmation' => Input::get('password_confirmation'),
            );

            $validator = Validator::make($passwords, User::$resetPasswordConfirmRules, User::getResetPasswordConfirmErrorMessages());
            if ($validator->fails()) {
                return Redirect::route('resetpassword_confirm', $code)->withInput()->withErrors($validator);
            }

            $credentials = array(
                'email' => $reminder->email,
                'password' => Input::get('password'),
                'password_confirmation' => Input::get('password_confirmation'),
                'token' => $code,
            );

            $response = Password::reset($credentials, function ($user, $password) {
                $user->password = Hash::make($password);
                $user->save();
            });

            switch ($response) {
                case Password::INVALID_PASSWORD:
                    return Redirect::route('resetpassword_confirm', $code)->with('msgError', Lang::get('user.password_incorrect'));

                case Password::PASSWORD_RESET:
                    return Redirect::route('home')->with('msgSuccess', Lang::get('user.password_reset_success'));
            }

            return Redirect::route('home')->with('msgError', Lang::get('user.confirmation_error'));
        }


        return View::make('user.resetpassword_confirm', array(
            'token' => $code,
        ));
    }

    /*-----------------------------------------------------------------------------*/

    /**
     * Send confirmation email to user
     *
     * @var User $user
     * @return void
     */
    protected function _sendConfirmationMail(User $user)
    {
        Mail::send('emails.auth.confirmation', array(
                'token' => $user->confirmation_token
            ),
            function ($message) use ($user) {
                $message->to($user->email)->subject(Lang::get('user.confirmation_email_subject'));
            }
        );
    }


}
