<?php

class VacancyController extends \BaseController
{

    protected $vacanciesPerPage = 10;

    public function __construct()
    {
        $this->beforeFilter('auth', array('only' => array('edit', 'delete', 'myVacancies', 'myVacanciesDrafts')));
    }

    /**
     * Vacancy edit action
     *
     * @var $id
     * @return Response
     */
    public function edit($id = 0)
    {
        $errors = array();
        $user = Auth::user();
        $vacancy = null;
        $form = Vacancy::getEmptyEditForm();
        $id = intval($id);

        // check vacancy exists
        if ($id > 0) {
            $vacancy = Vacancy::find($id);
            if (!$vacancy instanceof Vacancy || $vacancy->user->id != $user->id) {
                return Redirect::route('home')->with('msgError', Lang::get('vacancy.not_found'));
            }

            $form = $vacancy->getForm();
        }

        if (Request::isMethod('post')) {
            $form = Vacancy::getFormFromRequest();

            $form['delete_logo'] = Input::get('delete_logo');
            $form['delete_company_logo'] = Input::get('delete_company_logo');

            $validator = Validator::make($form, Vacancy::getEditRules($form), Vacancy::getEditErrorMessages());
            $errors = $validator->messages();

            if ($form['delete_logo'] == '1') {
                $form['logo'] = '';
            }
            if ($form['delete_company_logo'] == '1') {
                $form['logo'] = '';
            }

            $logo = '';
            if (is_object(Input::file('logo'))) {
                if (!$logo = Vacancy::saveLogo('logo')) {
                    $errors->add('logo', Lang::get('company.logo_error'));
                } else {
                    $form['logo'] = $logo;
                    $form['logo_cache'] = $logo;
                }
            } else {
                $form['logo'] = $form['logo_cache'];
            }

            $company_logo = '';
            if (is_object(Input::file('company_logo'))) {
                if (!$logo = Company::saveLogo('company_logo')) {
                    $errors->add('company_logo', Lang::get('company.logo_error'));
                } else {
                    $form['company_logo'] = $logo;
                    $form['company_logo_cache'] = $logo;
                }
            } else {
                $form['company_logo'] = $form['company_logo_cache'];
            }

            // save
            if (count($errors) == 0) {
                $isInsert = false;
                if (!$vacancy instanceof Vacancy) {
                    $vacancy = new Vacancy();
                    $form['created_at'] = time();
                    $isInsert = true;
                }

                $form['user_id'] = $user->id;

                //logo
                if ($form['delete_logo'] == '1' && !empty($vacancy->logo)) {
                    Company::deleteLogo($vacancy->logo);
                    $form['logo'] = '';
                }
                if (!empty($logo)) {
                    Company::deleteLogo($vacancy->logo);
                    $form['logo'] = $logo;
                }

                // company logo
                if ($form['delete_company_logo'] == '1' && !empty($vacancy->company_logo)) {
                    Company::deleteLogo($vacancy->company_logo);
                    $form['company_logo'] = '';
                }
                if (!empty($logo)) {
                    Company::deleteLogo($vacancy->company_logo);
                    $form['company_logo'] = $company_logo;
                }
                $vacancy->assignFormData($form);

                if ($isInsert) {
                    $vacancy->active = 0;
                }
                $vacancy->save();

                DB::table('vacancies_tags')->where('vacancy_id', '=', $vacancy->id)->delete();
                if (is_array($form['tags']) && count($form['tags']) > 0) {
                    $tags = array();
                    foreach ($form['tags'] as $tag) {
                        $tags[] = array('tag_id' => $tag, 'vacancy_id' => $vacancy->id);
                    }
                    DB::table('vacancies_tags')->insert($tags);
                }

                if ($isInsert) {
                    if ($user->getBalance() < Vacancy::$publishCost) {
                        return Redirect::route('vacancy_edit', $vacancy->id)->with('msgWarning', Lang::get('vacancy.vacancy_saved_to_drafts'));
                    }

                    $vacancy->active = 1;
                    $vacancy->save();
                    $user->decrementBalance(Vacancy::$publishCost);
                }

                return Redirect::route('vacancy_edit', $vacancy->id)->with('msgSuccess', Lang::get('vacancy.vacancy_saved'));
            }
        }

        if (!empty($form['logo'])) $form['logo_cache'] = $form['logo'];
        if (!empty($form['company_logo'])) $form['company_logo_cache'] = $form['company_logo'];

        return View::make('vacancy.edit')
            ->with('form', $form)
            ->with('id', $id)
            ->withErrors($errors);
    }

    /**
     * Vacancy delete action
     *
     * @return Response
     */
    public function delete()
    {

    }

    /**
     * Vacancy show action
     *
     * @var $id
     * @var $slug
     * @return Response
     */
    public function show($id, $slug)
    {
        $vacancy = Vacancy::where('id', '=', $id)
            ->where('created_at', '>', time() - Vacancy::getLiveTime())
            ->first();
        if (!$vacancy instanceof Vacancy) App::abort(404);

        return View::make('vacancy.show')->with('vacancy', $vacancy);
    }

    /**
     * Show my vacancies (active)
     *
     * @return Response
     */
    public function myVacancies()
    {
        $type = Input::get('type');
        if ($type != 'drafts') $type = '';

        $vacancies = Vacancy::where('user_id', Auth::user()->id)
            ->where('active', empty($type))
            ->paginate($this->vacanciesPerPage);

        return View::make('vacancy.my')
            ->with('vacancies', $vacancies)
            ->with('type', $type);
    }

    /**
     * Show my vacancies (drafts)
     *
     * @return Response
     */
    public function myVacanciesDrafts()
    {

    }

}
