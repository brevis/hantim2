<?php

class CmsController extends \BaseController
{

    /**
     * Show page action
     *
     * @return Response
     */
    public function showPage($slug)
    {
        $page = DB::table('pages')->where('slug', '=', $slug)->first();
        if (!is_object($page)) App::abort(404);

        return View::make('cms.page')->with('page', $page);
    }

}
