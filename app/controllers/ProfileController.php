<?php

class ProfileController extends \BaseController
{

    /** @var integer */
    protected $balanceAmount = 10;

    public function __construct()
    {
        $this->beforeFilter('auth');
    }

    /**
     * Profile action
     *
     * @return Response
     */
    public function profile()
    {
        $errors = array();

        $form = array(
            'email' => Auth::user()->email,
            'first_name' => Auth::user()->first_name,
            'last_name' => Auth::user()->last_name,
        );

        if (Request::isMethod('post')) {
            $form = Input::get('form');

            // add current user ID to form validation rules
            $rules = User::$profileRules;
            $rules['email'] .= Auth::user()->id;

            $validator = Validator::make($form, $rules, User::getProfileErrorMessages());
            if (!$validator->fails()) {
                $user = Auth::user();
                $user->email = $form['email'];
                $user->first_name = $form['first_name'];
                $user->last_name = $form['last_name'];
                $user->save();

                return Redirect::route('profile')->with('msgSuccess', Lang::get('user.profile_saved'));
            }

            $errors = $validator->messages();
        }

        return View::make('profile.settings')->with('form', $form)->withErrors($errors);
    }

    /**
     * Profile balance action
     *
     * @return Response
     */
    public function profileBalance()
    {
        if (Request::isMethod('post')) {
            $code = Input::get('code');
            $captcha = Session::get('captcha_balance');
            Session::put('captcha_balance', '');

            if (empty($code) || $code != $captcha) {
                return Redirect::route('profile_balance')->withErrors(array(
                    'code' => Lang::get('user.captcha_code_wrong')
                ));
            }

            $balance = DB::table('balance')->where('user_id', Auth::user()->id)->first();
            if (!is_object($balance)) {
                DB::table('balance')->insert(array(
                    'user_id' => Auth::user()->id,
                    'balance' => $this->balanceAmount,
                ));
            } else {
                DB::table('balance')->where('user_id', Auth::user()->id)->update(array(
                    'balance' => $balance->balance += $this->balanceAmount,
                ));
            }

            return Redirect::route('profile_balance')->with('msgSuccess', Lang::get('user.balance_changed'));
        }

        Session::put('captcha_balance', '');
        return View::make('profile.balance')->with('balanceAmount', $this->balanceAmount);
    }

    /**
     * Favorites action
     *
     * @return Response
     */
    public function favorites()
    {
        return View::make('profile.favorites');
    }

}
