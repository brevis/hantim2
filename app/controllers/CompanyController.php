<?php

class CompanyController extends \BaseController
{

    protected $companiesPerPage = 10;
    protected $vacanciesPerPage = 10;

    public function __construct()
    {
        $this->beforeFilter('auth', array('only' => array('edit', 'delete')));
    }

    /**
     * Company edit action
     *
     * @return Response
     */
    public function edit()
    {
        $errors = array();
        $user = Auth::user();
        $company = null;
        $form = Company::getEmptyEditForm();

        try {
            $id = intval($user->company->id);
        } catch (\Exception $e) {
            $id = 0;
        }

        // check company exists
        if ($id > 0) {
            $company = Company::find($id);
            if (!$company instanceof Company || $company->user->id != $user->id) {
                return Redirect::route('home')->with('msgError', Lang::get('company.not_found'));
            }

            $form = $company->getForm();
        }

        if (Request::isMethod('post')) {
            $form = Company::getFormFromRequest();

            $form['delete_logo'] = Input::get('delete_logo');

            $validator = Validator::make($form, Company::getEditRules($id), Company::getEditErrorMessages());
            $errors = $validator->messages();

            // check created_at
            if (!Company::checkDate($form)) {
                $errors->add('date', Lang::get('company.wrong_date'));
            }

            if ($form['delete_logo'] == '1') {
                $form['logo'] = '';
            }

            $logo = '';
            if (is_object(Input::file('logo'))) {
                if (!$logo = Company::saveLogo('logo')) {
                    $errors->add('logo', Lang::get('company.logo_error'));
                } else {
                    $form['logo'] = $logo;
                    $form['logo_cache'] = $logo;
                }
            } else {
                $form['logo'] = $form['logo_cache'];
            }

            // save
            if (count($errors) == 0) {
                if (!$company instanceof Company) {
                    $company = new Company();
                }

                if (intval($form['employments_count']) < 1) $form['employments_count'] = '';

                $form['user_id'] = $user->id;
                if ($form['delete_logo'] == '1' && !empty($company->logo)) {
                    Company::deleteLogo($company->logo);
                    $form['logo'] = '';
                }
                if (!empty($logo)) {
                    Company::deleteLogo($company->logo);
                    $form['logo'] = $logo;
                }
                $company->assignFormData($form);
                $company->save();

                return Redirect::route('company_edit')->with('msgSuccess', Lang::get('company.company_saved'));
            }
        }

        if (!empty($form['logo'])) $form['logo_cache'] = $form['logo'];

        return View::make('company.edit')
            ->with('form', $form)
            ->with('id', $id)
            ->withErrors($errors);
    }

    /**
     * Company delete action
     *
     * @return Response
     */
    public function delete()
    {

    }

    /**
     * Company show action
     *
     * @var $slug
     * @return Response
     */
    public function show($slug)
    {
        $company = Company::where('slug', '=', $slug)->first();
        if (!$company instanceof Company) App::abort(404);

        return View::make('company.show')->with('company', $company);
    }

    /**
     * Show all companies action
     *
     * @return Response
     */
    public function showAll()
    {
        $s = Input::get('s');
        if (!empty($s)) {
            $companies = Company::where('name', 'LIKE', '%' . $s . '%')->orderBy('name', 'asc')->paginate($this->companiesPerPage);
        } else {
            $companies = Company::orderBy('name', 'asc')->paginate($this->companiesPerPage);
        }

        return View::make('company.all')->with('companies', $companies)->with('s', $s);
    }

    /**
     * Company vacancies action
     *
     * @var @slug
     * @return Response
     */
    public function vacancies($slug)
    {
        $company = Company::where('slug', '=', $slug)->first();
        if (!$company instanceof Company) App::abort(404);

        $vacancies = Vacancy::where('company_id', '=', $company->id)
            ->where('active', 1)
            ->paginate($this->vacanciesPerPage);

        return View::make('company.vacancies')
            ->with('company', $company)
            ->with('vacancies', $vacancies);
    }

}
