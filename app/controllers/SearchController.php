<?php

class SearchController extends \BaseController
{

    protected $vacanciesPerPage = 30;

    /**
     * Vacancy search action
     *
     * @return Response
     */
    public function search()
    {
        $types = array_filter(explode(',', Input::get('type')));
        $categories = array_filter(explode(',', Input::get('category')));
        $kw = trim(Input::get('kw'));
        $location = trim(Input::get('city'));

        // search query
        $qb = Vacancy::where('active', 1);
        if (count($types) > 0) $qb->whereIn('type', $types);
        if (count($categories) > 0) {
            $qb->join('categories', function ($join) use ($categories) {
                $join->on('vacancies.category_id', '=', 'categories.id');
            });
            $qb->whereIn('categories.slug', $categories);
        }
        if (!empty($kw)) $qb->where('name', 'LIKE', '%' . $kw . '%');
        if (!empty($location)) {
            $qb->join('locations', function ($join) use ($location) {
                $join->on('vacancies.location_id', '=', 'locations.id');
            });
            $qb->where('locations.name', 'LIKE', $location . '%');
        }

        $qb->select('vacancies.*');

        $vacancies = $qb->orderBy('created_at', 'desc')->paginate($this->vacanciesPerPage);

        $tempate = 'search.index';
        if (Input::isXmlHttpRequest()) $tempate = 'search.ajax';

        return View::make($tempate)
            ->with('vacancies', $vacancies)
            ->with('types', $types)
            ->with('categories', $categories)
            ->with('kw', $kw)
            ->with('location', $location);
    }

}
