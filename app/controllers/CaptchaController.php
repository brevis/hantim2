<?php

class CaptchaController extends \BaseController
{

    /**
     * Captcha action
     */
    public function captcha($key)
    {
        $image = imagecreatetruecolor(180, 60);

        // fill background
        $bgColor = imagecolorallocate($image, 200, 200, 200);
        imagefill($image, 0, 0, $bgColor);

        // draw digits
        $text = strval(mt_rand(10000, 99999));
        Session::put('captcha_' . trim($key), $text);
        $font = __DIR__ . '/../../public/fonts/PWPerspective.ttf';
        $color = imagecolorallocate($image, 200, 20, 20);
        imagettftext($image, 36, 0, 11, 45, $color, $font, $text);

        // return image
        header('Content-Type: image/png');
        Response::make('', 200, array(
            'Content-Type' => 'image/png',
            'Content-Transfer-Encoding' => 'binary',
            'Content-Disposition' => 'inline',
            'Expires' => 0,
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Pragma' => 'public',
        ));
        imagepng($image);
    }

}
