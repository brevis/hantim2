<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@section('site_title')
        Вакансии &mdash; {{ Lang::get('site.logo') }}
        @show</title>
    {{ HTML::style('css/bootstrap.min.css') }}
    {{ HTML::style('chosen/chosen.css') }}
    {{ HTML::style('css/hantim2.css?2') }}
    <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">
    <!--[if lt IE 9]>
    {{ HTML::script('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') }}
    <![endif]-->
</head>
<body>

@section('header')
<div class="header col-md-12 well">
    <h1><a href="{{ route('home') }}">{{ Lang::get('site.logo') }}</a></h1>

    @if(Auth::check())
    <ul class="links">
        <li><i class="glyphicon glyphicon-book"></i> <a href="{{ route('myvacancies') }}">Мои вакансии</a></li>
        @if ( Auth::user()->hasCompany() )
        <li><i class="glyphicon glyphicon-lock"></i> <a href="{{ route('company_edit') }}">Моя компания</a></li>
        @endif
        <li><i class="glyphicon glyphicon-star"></i> <a onclick="alert('Coming soon');return false;"
                                                        href="{{ route('favorites') }}">Избранное</a></li>
    </ul>

    <div class="loggedas">
        <div>{{ Auth::user()->email }} <a href="{{ route('logout') }}" class="logout"><i
                    class="glyphicon glyphicon-log-out"></i></div>
        <div class="links">
            <a href="{{ route('profile_balance') }}">{{ Lang::get('profile.balance') }}: <span class="badge">{{ Auth::user()->getBalance() }}</span></a>
            <a href="{{ route('profile') }}">{{ Lang::get('site.settings') }}</a>
        </div>
    </div>
    @else
    <blockquote>&mdash;<span>Работа не волк: в лес не убежит.</span></blockquote>

    <div class="auth">
        <a href="{{ route('login') }}" class="login text-success">{{ Lang::get('user.enter_action') }}</a>
        <a href="{{ route('register') }}" class="register">{{ Lang::get('user.registration') }}</a>
    </div>
    @endif

</div>
@show

@yield('content')

@section('footer')
<div class="footer col-md-12">
    <div class="links">
        <a href="{{ route('page', 'about') }}">О проекте</a>
        <a href="{{ route('page', 'services') }}">Услуги</a>
        <a href="{{ route('companies') }}">Компании</a>
        <a href="{{ route('page', 'faq') }}">FAQ</a>
        <a href="{{ route('page', 'terms') }}">Соглашение</a>
    </div>
    <div class="copyright">
        <span class="copyleft">&copy;</span> 2014 <a href="{{ route('home') }}">{{ Lang::get('site.logo') }}</a>.
    </div>
</div>
@show

{{ HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js') }}
{{ HTML::script('js/bootstrap.min.js') }}
{{ HTML::script('chosen/chosen.jquery.js') }}
{{ HTML::script('js/hantim2.js?2') }}
</body>
</html>