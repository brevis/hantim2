@extends('master')

@section('content')
<div class="content col-md-12 well">
    <div class="col-md-9 main">

        @include('flashmessage')

        <div class="searchform">
            <form action="{{ route('home') }}" method="get">
                <div class="col-md-6 keyword">
                    <input type="text" class="form-control" id="filter_kw" placeholder="Ключевое слово"
                           value="{{{ $kw }}}">
                    <i class="categories_trigger glyphicon glyphicon-cog{{ count($categories) > 0 ? ' hl' : '' }}"></i>

                    <div class="filter-categories">
                        <div class="arrow-up"></div>
                        @foreach(Category::getAll() as $category)
                        <label for="category_{{ $category->slug }}">
                            <div class="check"><input id="category_{{ $category->slug }}" class="vacancy_category"
                                                      name="category[]" type="checkbox" value="{{ $category->slug }}"{{
                                in_array($category->slug, $categories) ? ' checked="checked"' : '' }}>
                            </div>
                            <div class="title">{{ $category->name }}</div>
                            <div class="description">{{ $category->description }}</div>
                        </label>
                        @endforeach

                    </div>

                </div>
                <div class="col-md-6 location">
                    <input type="text" class="form-control" id="filter_location" placeholder="Город"
                           value="{{{ $location }}}">
                </div>
                <div style="clear: both;"></div>
            </form>

            <div>
                <label style="cursor: pointer;"><input type="checkbox" class="vacancy_type" value="fulltime"{{
                    in_array('fulltime', $types) ? ' checked="checked"' : '' }}> <span class="label label-primary">Фултайм</span></label>
                <label style="cursor: pointer;"><input type="checkbox" class="vacancy_type" value="contract"{{
                    in_array('contract', $types) ? ' checked="checked"' : '' }}> <span class="label label-success">Контракт</span></label>
                <label style="cursor: pointer;"><input type="checkbox" class="vacancy_type" value="freelance"{{
                    in_array('freelance', $types) ? ' checked="checked"' : '' }}> <span
                        class="label label-info">Фриланс</span></label>
            </div>
        </div>

        <div class="jobslist">
            <div id="searchResult">
                @foreach ($vacancies as $vacancy)

                {{--
                <a href="{{ route('vacancy', array($i, $i)) }}" class="job featured" target="_blank">
                    --}}

                    <a href="{{ $vacancy->getUrl() }}" class="job" target="_blank">

                        {{--
                        <div class="favorite active">
                            <i class="glyphicon glyphicon-star" onclick="alert('Coming soon');return false;"></i>
                        </div>
                        --}}

                        <div class="favorite">
                            <i class="glyphicon glyphicon-star-empty" onclick="alert('Coming soon');return false;"></i>
                        </div>

                        <div class="icon">
                            {{ $vacancy->getLogoWidget() }}
                        </div>
                        <div class="title">
                            <h3>{{{ $vacancy->name }}}</h3>
                            <h4>{{{ $vacancy->getCompanyName() }}}</h4>
                        </div>

                        <div class="type">
                            @if ($vacancy->type == 'fulltime')
                            <span class="label label-primary">Фултайм</span>
                            @elseif ($vacancy->type == 'contract')
                            <span class="label label-success">Контракт</span>
                            @else
                            <span class="label label-info">Фриланс</span>
                            @endif

                        </div>

                        @if( !empty($vacancy->location_id) )
                        <div class="location">
                            <i class="glyphicon glyphicon-map-marker"></i> {{ $vacancy->getLocationName() }}
                        </div>
                        @endif
                    </a>
                    @endforeach

                    {{ $vacancies->links() }}

            </div>

            {{--
            <a href="" class="showmore"><span>Показать ещё</span></a>
            --}}
        </div>

    </div>

    <div class="col-md-3 sidebar">
        @if(Auth::check())
        <a href="{{ asset('/') }}vacancy/new" class="btn btn-primary">Добавить вакансию</a>
        @endif

        <div class="companies">
            @foreach (Company::getRandomCompanies(5) as $company)
            <a href="{{ route('company', $company->slug) }}" class="company">
                <span class="badge">{{ $company->getVacanciesCount() }}</span>
                <img src="{{ asset('uploads/companies') }}/medium/{{ $company->logo }}" alt="{{{ $company->name }}}">
            </a>
            @endforeach
        </div>
        <a href="{{ route('companies') }}" class="allcompanies">Все компании</a>

        <!--
        <div class="infoblock">
            <h2>МЫ ПРИНИМАЕМ</h2>
            <p>
                <img src="http://hantim.ru/assets/creditcards-110206b8cdc2b86140bd0bdad9d845ea.png" alt="">
            </p>
        </div>
        -->

    </div>
</div>

<script>
    var searchParams = {
        url: '{{ route('home') }}',
        category: "",
        type: "",
        kw: "",
        city: ""
    };
</script>
@stop