@foreach ($vacancies as $vacancy)

{{--
<a href="{{ route('vacancy', array($i, $i)) }}" class="job featured" target="_blank">
    --}}

    <a href="{{ $vacancy->getUrl() }}" class="job" target="_blank">

        {{--
        <div class="favorite active">
            <i class="glyphicon glyphicon-star" onclick="alert('Coming soon');return false;"></i>
        </div>
        --}}

        <div class="favorite">
            <i class="glyphicon glyphicon-star-empty" onclick="alert('Coming soon');return false;"></i>
        </div>

        <div class="icon">
            {{ $vacancy->getLogoWidget() }}
        </div>
        <div class="title">
            <h3>{{{ $vacancy->name }}}</h3>
            <h4>{{{ $vacancy->getCompanyName() }}}</h4>
        </div>

        <div class="type">
            @if ($vacancy->type == 'fulltime')
            <span class="label label-primary">Фултайм</span>
            @elseif ($vacancy->type == 'contract')
            <span class="label label-success">Контракт</span>
            @else
            <span class="label label-info">Фриланс</span>
            @endif

        </div>

        @if( !empty($vacancy->location_id) )
        <div class="location">
            <i class="glyphicon glyphicon-map-marker"></i> {{ $vacancy->getLocationName() }}
        </div>
        @endif
    </a>
    @endforeach

    {{ $vacancies->links() }}
