@extends('master')

@section('site_title')
Компании
@stop

@section('content')
<div class="content col-md-12 well">
    <div class="col-md-9 main">

        <h3>Компании</h3>

        <form action="{{ route('companies') }}" class="companies-filter" method="get" id="frmCompanies">
            <div class="form-group">
                <input type="text" class="form-control" name="s" id="companyKw" maxlength="20" value="{{{ $s }}}">
                <i class="glyphicon glyphicon-search"></i>
            </div>
        </form>

        <div class="jobslist companies" style="margin-top: 40px;">
            @foreach ($companies as $company)
            <a href="{{ route('company', $company->slug) }}" class="job">
                <div class="icon">
                    @if( !empty($company->logo) )
                    <img src="{{ asset('uploads/companies') }}/small/{{ $company->logo }}" alt="{{{ $company->name }}}">
                    @endif
                </div>
                <div class="title">
                    <h3>{{{ $company->name }}}</h3>
                </div>
                <div class="count">
                    <b>{{ $company->getVacanciesCount() }}</b>
                    <span>вакансий</span>
                </div>
            </a>
            @endforeach
        </div>

        {{ $companies->links() }}
        <!--
	    <ul class="pagination">
	    	<li><a href="">&lt;&lt;</a></li>
	    	<li class="current"><span>1</span></li>
	    	<li><a href="">2</a></li>
	    	<li><a href="">3</a></li>
	    	<li><span>...</span></li>
	    	<li><a href="">&gt;&gt;</a></li>
	    </ul>
	    -->

    </div>

    <div class="col-md-3 sidebar">

        @if( Auth::check() && !Auth::user()->hasCompany() )
        <a href="{{ route('company_edit') }}" class="btn btn-primary">Создать компанию</a>
        @endif

    </div>
</div>
@stop