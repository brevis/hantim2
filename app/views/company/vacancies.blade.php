@extends('master')

@section('site_title')
OUMOBILE
@stop

@section('content')
<div class="content col-md-12 well">
    <div class="col-md-9 main">

        <ul class="nav nav-pills">
            <li><a href="{{ route('company', $company->slug) }}">О компании</a></li>
            <li class="active"><a href="{{ route('company', $company->slug) }}">Вакансии <span class="badge">{{ $company->getVacanciesCount() }}</span></a>
            </li>
        </ul>

        <div class="jobslist" style="margin-top: 40px;">
            @foreach ($vacancies as $vacancy)

            {{--
            <a href="{{ route('vacancy', array($i, $i)) }}" class="job featured">
                --}}

                <a href="{{ $vacancy->getUrl() }}" class="job">

                    {{--
                    <div class="favorite active">
                        <i class="glyphicon glyphicon-star" onclick="alert('ok');return false;"></i>
                    </div>
                    --}}

                    <div class="favorite">
                        <i class="glyphicon glyphicon-star-empty" onclick="return false;"></i>
                    </div>

                    <div class="icon">
                        {{ $vacancy->getLogoWidget() }}
                    </div>
                    <div class="title">
                        <h3>{{{ $vacancy->name }}}</h3>
                        <h4>{{{ $vacancy->getCompanyName() }}}</h4>
                    </div>

                    <div class="type">
                        @if ($vacancy->type == 'fulltime')
                        <span class="label label-primary">Фултайм</span>
                        @elseif ($vacancy->type == 'contract')
                        <span class="label label-success">Контракт</span>
                        @else
                        <span class="label label-info">Фриланс</span>
                        @endif

                    </div>

                    @if( !empty($vacancy->location_id) )
                    <div class="location">
                        <i class="glyphicon glyphicon-map-marker"></i> {{ $vacancy->getLocationName() }}
                    </div>
                    @endif
                </a>
                @endforeach
        </div>

        {{ $vacancies->links() }}

    </div>

    <div class="col-md-3 sidebar">

        <div class="infoblock">
            <h2><a href="{{ route('company', $company->slug) }}">{{{ $company->name }}}</a></h2>
            @if( !empty($company->logo) )
            <p>
                <a href="{{ route('company', $company->slug) }}"><img
                        src="{{ asset('uploads/companies') }}/big/{{ $company->logo }}" alt=""></a>
            </p>
            @endif

            <p class="description">
                @if( $company->getLocationName() != '' )
                <i class="glyphicon glyphicon-map-marker"></i> {{ $company->getLocationName() }}<br>
                @endif
                @if( !empty($company->url) )
                <i class="glyphicon glyphicon-link"></i> <a href="{{ $company->url }}" target="_blank">{{ $company->url
                    }}</a><br>
                @endif
            </p>

            @if( !empty($company->address) )
            <h3 class="addr">Адреса</h3>

            <p>
                <em>Основной офис</em><br>
                {{{ $company->address }}}
            </p>
            @endif
        </div>

    </div>
</div>
@stop