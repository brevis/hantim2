@extends('master')

@section('site_title')
{{ empty($id) ? Lang::get('company.creating') : Lang::get('company.editing') }}
@stop

@section('content')
<div class="content col-md-12 well">
    <div class="col-md-9 main">

        <div class="company_edit">

            <h2>
                <div style="float: left;">
                    {{ empty($id) ? Lang::get('company.creating') : Lang::get('company.editing') }}
                </div>
                <div style="float: right;">
                    <a href="{{ route('company_delete') }}" class="btn btn-danger btn-mini"
                       onclick="return confirm('{{ Lang::get('company.are_you_sure_delete') }}');">{{
                        Lang::get('company.delete') }}</a>
                </div>
                <div style="clear: both;"></div>
            </h2>

            @include('flashmessage')

            @if (isset($errors) && count($errors)>0)
            <div class="alert alert-danger">
                {{ Lang::get('site.errors_happens') }}
            </div>
            @endif

            {{ Form::open(array('url' => route('company_edit'), 'method' => 'post', 'enctype' => 'multipart/form-data'))
            }}
            <div class="block">

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="col-lg-12 control-label" for="name">{{ Lang::get('company.name') }}<b
                            class="required">*</b></label>
                    <input class="form-control" id="name" name="form[name]" maxlength="50" type="text"
                           value="{{{ $form['name'] }}}">

                    <p class="text-danger">{{ $errors->first('name') }}</p>
                </div>

                <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                    <label class="col-lg-12 control-label" for="slug">{{ Lang::get('company.slug') }}<b
                            class="required">*</b></label>
                    <input class="form-control" id="slug" name="form[slug]" maxlength="50" type="text"
                           value="{{{ $form['slug'] }}}">

                    <p class="text-danger">{{ $errors->first('slug') }}</p>
                </div>

                <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                    <label class="col-lg-12 control-label">{{ Lang::get('company.created') }}</label>
                    <select class="" id="created_at_day" name="form[day]">
                        <option value="">{{ Lang::get('company.day') }}</option>
                        @for ($i = 1; $i <= 31; $i++)
                        <option value="{{ $i }}"
                        {{ isset($form['day']) && $form['day']==$i ? ' selected="selected"' : '' }}>{{ $i }}</option>
                        @endfor
                    </select>
                    <select class="" id="created_at_month" name="form[month]">
                        <option value="">{{ Lang::get('company.month') }}</option>
                        @for ($i = 1; $i <= 12; $i++)
                        <option value="{{ $i }}"
                        {{ isset($form['month']) && $form['month']==$i ? ' selected="selected"' : '' }}>{{ $i
                        }}</option>
                        @endfor
                    </select>

                    <select class="" id="created_at_year" name="form[year]">
                        <option value="">{{ Lang::get('company.year') }}</option>
                        @for ($i = date('Y'); $i >= 1960; $i--)
                        <option value="{{ $i }}"
                        {{ isset($form['year']) && $form['year']==$i ? ' selected="selected"' : '' }}>{{ $i }}</option>
                        @endfor
                    </select>

                    <p class="text-danger">{{ $errors->first('date') }}</p>
                </div>

                <div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
                    <label class="col-lg-12 control-label" for="url">{{ Lang::get('company.url') }}</label>
                    <input class="form-control" id="url" name="form[url]" maxlength="100" type="text"
                           value="{{{ $form['url'] }}}">

                    <p class="text-danger">{{ $errors->first('url') }}</p>
                </div>

                <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">
                    <label class="col-lg-12 control-label" for="logo">{{ Lang::get('company.logo') }}</label>
                    @if( $form['logo_cache'] != '' )
                    <div id="logo_preview">
                        <input id="logo_cache" name="form[logo_cache]" type="hidden"
                               value="{{{ $form['logo_cache'] }}}">
                        <img src="{{ asset('uploads/companies/small') }}/{{{ $form['logo_cache'] }}}" alt="">
                        <button type="button"
                                onclick="$('#delete_logo').val('1');$('#logo_cache').val('');$('#logo_preview').hide();"
                                class="btn btn-mini minibtn btn-danger">delete
                        </button>
                    </div>
                    @endif
                    <input class="fileinput" id="logo" name="logo" type="file">
                    <input id="delete_logo" name="delete_logo" type="hidden" value="{{{ $form['delete_logo'] }}}">

                    <p class="text-danger">{{ $errors->first('logo') }}</p>
                </div>

            </div>

            <div class="block">

                <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                    <label class="col-lg-12 control-label" for="location_id">{{ Lang::get('company.location') }}</label>
                    <select class="form-control chosen" id="location_id" name="form[location_id]">
                        <option value=""></option>
                        @foreach (Location::getAll() as $location)
                        <option value="{{ $location->id }}"
                        {{ ($form['location_id']==$location->id) ? ' selected="selected"' : '' }}>{{{ $location->name
                        }}}</option>
                        @endforeach
                    </select>

                    <p class="text-danger">{{ $errors->first('location') }}</p>
                </div>

                <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                    <label class="col-lg-12" for="address">{{ Lang::get('company.address') }}</label>
                    <input class="form-control" id="address" name="form[address]" maxlength="100" type="text"
                           value="{{{ $form['address'] }}}">

                    <p class="text-danger">{{ $errors->first('address') }}</p>
                </div>

            </div>

            <div class="block">

                <div class="form-group{{ $errors->has('employments_count') ? ' has-error' : '' }}">
                    <label class="col-lg-12 control-label" for="employments_count">{{
                        Lang::get('company.employments_count') }}</label>

                    <div class="col-lg-2" style="padding: 0;">
                        <input class="form-control" style="margin: 0;" id="employments_count"
                               name="form[employments_count]" maxlength="5" type="text"
                               value="{{{ $form['employments_count'] }}}"">
                    </div>
                    <div style="clear: both;"></div>
                    <p class="text-danger">{{ $errors->first('employments_count') }}</p>
                </div>

                <div class="form-group{{ $errors->has('description_short') ? ' has-error' : '' }}">
                    <label class="col-lg-12 control-label" for="description_short">{{
                        Lang::get('company.description_short') }}</label>
                    <textarea id="description_short" style="height: 80px;" class="form-control"
                              name="form[description_short]">{{{ $form['description_short'] }}}</textarea>

                    <p class="text-danger">{{ $errors->first('description_short') }}</p>
                </div>

                <div class="form-group{{ $errors->has('description_full') ? ' has-error' : '' }}">
                    <label class="col-lg-12 control-label" for="description_full">{{
                        Lang::get('company.description_full') }}</label>
                    <textarea id="description_full" style="height: 140px;" class="form-control"
                              name="form[description_full]">{{{ $form['description_full'] }}}</textarea>

                    <p class="text-danger">{{ $errors->first('description_full') }}</p>
                </div>

                <div class="form-group{{ $errors->has('contact_person') ? ' has-error' : '' }}">
                    <label class="col-lg-12 control-label" for="contact_person">{{ Lang::get('company.contact_person')
                        }}</label>

                    <div class="col-lg-4" style="padding: 0;">
                        <input class="form-control" style="margin: 0;" id="contact_person" name="form[contact_person]"
                               maxlength="20" type="text" value="{{{ $form['contact_person'] }}}">
                    </div>
                    <div style="clear: both;"></div>
                    <p class="text-danger">{{ $errors->first('contact_person') }}</p>
                </div>

                <div class="form-group{{ $errors->has('contact_phone') ? ' has-error' : '' }}">
                    <label class="col-lg-12 control-label" for="contact_phone">{{ Lang::get('company.contact_phone')
                        }}</label>

                    <div class="col-lg-4" style="padding: 0;">
                        <input class="form-control" style="margin: 0;" id="contact_phone" name="form[contact_phone]"
                               maxlength="20" type="text" value="{{{ $form['contact_phone'] }}}">
                    </div>
                    <div style="clear: both;"></div>
                    <p class="text-danger">{{ $errors->first('contact_phone') }}</p>
                </div>

            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">{{ Lang::get('site.save') }}</button>
            </div>

            {{ Form::close() }}

        </div>

    </div>

    <div class="col-md-3 sidebar">


    </div>
</div>
@stop