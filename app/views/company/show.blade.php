@extends('master')

@section('site_title')
{{{ $company->name }}}
@stop

@section('content')
<div class="content col-md-12 well">
    <div class="col-md-9 main">

        <ul class="nav nav-pills">
            <li class="active"><a href="{{ route('company', $company->slug) }}">О компании</a></li>
            <li><a href="{{ route('company_vacancies', $company->slug) }}">Вакансии <span class="badge">{{ $company->getVacanciesCount() }}</span></a>
            </li>
        </ul>

        <div class="company_show">
            <dl>
                <dt>Расположение</dt>
                <dd>{{ $company->getLocationName() }}</dd>
            </dl>

            <dl>
                <dt>Кол-во сотрудников</dt>
                <dd>{{ $company->employments_count }}</dd>
            </dl>

            <dl>
                <dt>Описание</dt>
                <dd>
                    <div class="text">
                        {{ nl2br($company->description_full) }}
                    </div>
                </dd>
            </dl>
            <dl>
                <dt>Дата основания</dt>
                <dd>{{ $company->created }}</dd>
            </dl>
        </div>

    </div>

    <div class="col-md-3 sidebar">

        <div class="infoblock">
            <h2><a href="{{ route('company', $company->slug) }}">{{{ $company->name }}}</a></h2>
            @if( !empty($company->logo) )
            <p>
                <a href="{{ route('company', $company->slug) }}"><img
                        src="{{ asset('uploads/companies') }}/big/{{ $company->logo }}" alt=""></a>
            </p>
            @endif

            <p class="description">
                @if( $company->getLocationName() != '' )
                <i class="glyphicon glyphicon-map-marker"></i> {{ $company->getLocationName() }}<br>
                @endif
                @if( !empty($company->url) )
                <i class="glyphicon glyphicon-link"></i> <a href="{{ $company->url }}" target="_blank">{{ $company->url
                    }}</a><br>
                @endif
            </p>

            @if( !empty($company->address) )
            <h3 class="addr">Адреса</h3>

            <p>
                <em>Основной офис</em><br>
                {{{ $company->address }}}
            </p>
            @endif
        </div>

    </div>
</div>
@stop