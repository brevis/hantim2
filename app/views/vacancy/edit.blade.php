@extends('master')

@section('site_title')
{{ empty($id) ? 'Добавление вакансии' : 'Редактирование вакансии' }}
@stop

@section('content')
<div class="content col-md-12 well">
    <div class="col-md-9 main">

        <div class="company_edit">

            <h2>{{ empty($id) ? 'Добавление вакансии' : 'Редактирование вакансии' }}</h2>

            @include('flashmessage')

            @if (isset($errors) && count($errors)>0)
            <div class="alert alert-danger">
                {{ Lang::get('site.errors_happens') }}
            </div>
            @endif

            @if ( !Auth::user()->hasCompany() )
            <div class="alert alert-warning notice">
                <b>Планируете регулярно размещать вакансии на "][антим"?</b>

                <p>Создайте карточку компании и получите дополнительные возможности при размещении вакансий</p>

                <p><a href="{{ route('company_edit') }}" class="btn btn-danger">Создать компанию</a></p>
            </div>
            @endif

            @if( empty($id) )
            {{ Form::open(array('url' => route('vacancy_new'), 'method' => 'post', 'enctype' => 'multipart/form-data'))
            }}
            @else
            {{ Form::open(array('url' => route('vacancy_edit', $id), 'method' => 'post', 'enctype' =>
            'multipart/form-data')) }}
            @endif
            <input type="hidden" name="preview" id="preview" value="">

            <div class="block">

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="col-lg-12 control-label" for="name">Название<b class="required">*</b></label>
                    <input class="form-control" id="name" name="form[name]" maxlength="50" type="text"
                           value="{{{ $form['name'] }}}">

                    <p class="text-danger">{{ $errors->first('name') }}</p>
                </div>

                <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                    <label class="col-lg-12">Тип зянятости</label>
                    <label style="cursor: pointer;"><input type="radio" name="form[type]" value="fulltime"{{
                        empty($form['type']) || $form['type']=='fulltime' ? ' checked="checked"' : '' }}> <span
                            class="label label-primary">Фултайм</span></label>
                    <label style="cursor: pointer;"><input type="radio" name="form[type]" value="contract"{{
                        $form['type']=='contract' ? ' checked="checked"' : '' }}> <span class="label label-success">Контракт</span></label>
                    <label style="cursor: pointer;"><input type="radio" name="form[type]" value="freelance"{{
                        $form['type']=='freelance' ? ' checked="checked"' : '' }}> <span class="label label-info">Фриланс</span></label>

                    <p class="text-danger">{{ $errors->first('type') }}</p>
                </div>

                <div class="form-group vacancy_location{{ $errors->has('location_id') ? ' has-error' : '' }}">
                    <label class="col-lg-12 control-label" for="location_id">Местоположение</label>
                    <select class="chosen form-control" id="location_id" name="form[location_id]">
                        <option value=""></option>
                        @foreach (Location::getAll() as $location)
                        <option value="{{ $location->id }}"
                        {{ ($form['location_id']==$location->id) ? ' selected="selected"' : '' }}>{{{ $location->name
                        }}}</option>
                        @endforeach
                    </select>

                    <p class="text-danger">{{ $errors->first('location') }}</p>
                </div>

                <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                    <label class="col-lg-12 control-label" for="category_id">Категория<b class="required">*</b></label>
                    <select class="form-control" id="category_id" name="form[category_id]">
                        <option value=""></option>
                        @foreach (Category::getAll() as $category)
                        <option value="{{ $category->id }}"
                        {{ ($form['category_id']==$category->id) ? ' selected="selected"' : '' }}>{{{ $category->name
                        }}}</option>
                        @endforeach
                    </select>

                    <p class="text-danger">{{ $errors->first('category_id') }}</p>
                </div>

            </div>

            <div class="block">

                <div class="form-group vacancy_skills{{ $errors->has('tags') ? ' has-error' : '' }}">
                    <label class="col-lg-12 control-label" for="tags">Необходимые навыки</label>
                    <select class="chosen form-control" id="tags" name="form[tags][]" size="6" multiple="">
                        @foreach (Tag::getAll() as $tag)
                        <option value="{{ $tag->id }}"
                        {{ in_array($tag->id, $form['tags']) ? ' selected="selected"' : '' }}>{{{ $tag->name
                        }}}</option>
                        @endforeach
                    </select>

                    <p class="text-danger">{{ $errors->first('tags') }}</p>
                </div>

                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                    <label class="col-lg-12 control-label" for="description">Основные требования<b
                            class="required">*</b></label>
                    <textarea class="form-control" style="height: 140px;" id="description" name="form[description]">{{{
                        $form['description'] }}}</textarea>

                    <p class="text-danger">{{ $errors->first('description') }}</p>
                </div>

                <div class="form-group{{ $errors->has('bonuses') ? ' has-error' : '' }}">
                    <label class="col-lg-12 control-label" for="bonuses">Бонусы</label>
                    <input class="form-control" id="bonuses" name="form[bonuses]" maxlength="100" type="text"
                           value="{{{ $form['bonuses'] }}}">

                    <p class="text-danger">{{ $errors->first('bonuses') }}</p>
                </div>

                <div class="form-group{{ $errors->has('contact_person') ? ' has-error' : '' }}">
                    <label class="col-lg-12 control-label" for="contact_person">Контактное лицо<b class="required">*</b></label>
                    <input class="form-control" id="contact_person" name="form[contact_person]" maxlength="100"
                           type="text" value="{{{ $form['contact_person'] }}}">

                    <p class="text-danger">{{ $errors->first('contact_person') }}</p>
                </div>

                <div class="form-group{{ $errors->has('contact_email') ? ' has-error' : '' }}">
                    <label class="col-lg-12 control-label" for="contact_email">Почта для связи<b class="required">*</b></label>
                    <input class="form-control" id="contact_email" name="form[contact_email]" maxlength="100"
                           type="email" value="{{{ $form['contact_email'] }}}">

                    <p class="text-danger">{{ $errors->first('contact_email') }}</p>
                </div>

                <div class="form-group{{ $errors->has('instructions') ? ' has-error' : '' }}">
                    <label class="col-lg-12 control-label" for="instructions">Инструкции</label>
                    <textarea class="form-control" id="instructions" name="form[instructions]">{{{ $form['instructions']
                        }}}</textarea>

                    <p class="text-danger">{{ $errors->first('instructions') }}</p>
                </div>

                <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">
                    <label class="col-lg-12 control-label" for="logo">Иконка вакансии</label>
                    @if( $form['logo_cache'] != '' )
                    <div id="logo_preview">
                        <input id="logo_cache" name="form[logo_cache]" type="hidden"
                               value="{{{ $form['logo_cache'] }}}">
                        <img src="{{ asset('uploads/vacancies') }}/{{{ $form['logo_cache'] }}}" alt="">
                        <button type="button"
                                onclick="$('#delete_logo').val('1');$('#logo_cache').val('');$('#logo_preview').hide();"
                                class="btn btn-mini minibtn btn-danger">delete
                        </button>
                    </div>
                    @endif
                    <input class="fileinput" id="logo" name="logo" type="file">
                    <input id="delete_logo" name="delete_logo" type="hidden" value="{{{ $form['delete_logo'] }}}">

                    <p class="text-danger">{{ $errors->first('logo') }}</p>
                </div>

            </div>

            <div class="block">
                <input type="hidden" id="company_id" data-company-id="{{ Auth::user()->getCompanyId() }}"
                       name="form[company_id]" value="{{{ $form['company_id'] }}}">

                <div class="form-group" id="connect_company_container"
                     style="display: {{ Auth::user()->hasCompany() && empty($form['company_id']) ? 'block' : 'none' }}">
                    <a href="#" class="connect_company" style="border-bottom: 1px dashed;text-decoration: none;">Связать
                        вакансию с компанией</a>
                </div>

                <div class="form-group" id="deconnect_company_container"
                     style="display: {{ !empty($form['company_id']) ? 'block' : 'none' }}">
                    <a href="#" class="deconnect_company" style="border-bottom: 1px dashed;text-decoration: none;">Отвязать
                        компанию</a>
                </div>

                <div id="companyForm" style="display: {{ empty($form['company_id']) ? 'block' : 'none' }}">
                    <div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
                        <label class="col-lg-12 control-label" for="company_name">Название компании<b
                                class="required">*</b></label>
                        <input class="form-control" id="company_name" name="form[company_name]" maxlength="50"
                               type="text" value="{{{ $form['company_name'] }}}">

                        <p class="text-danger">{{ $errors->first('company_name') }}</p>
                    </div>

                    <div class="form-group{{ $errors->has('company_logo') ? ' has-error' : '' }}">
                        <label class="col-lg-12 control-label" for="company_logo">Логотип компании</label>
                        @if( $form['company_logo_cache'] != '' )
                        <div id="company_logo_preview">
                            <input id="company_logo_cache" name="form[company_logo_cache]" type="hidden"
                                   value="{{{ $form['company_logo_cache'] }}}">
                            <img src="{{ asset('uploads/companies/small') }}/{{{ $form['company_logo_cache'] }}}"
                                 alt="">
                            <button type="button"
                                    onclick="$('#delete_company_logo').val('1');$('#company_logo_cache').val('');$('#company_logo_preview').hide();"
                                    class="btn btn-mini minibtn btn-danger">delete
                            </button>
                        </div>
                        @endif
                        <input class="fileinput" id="company_logo" name="company_logo" type="file">
                        <input id="delete_company_logo" name="delete_company_logo" type="hidden"
                               value="{{{ $form['delete_company_logo'] }}}">

                        <p class="text-danger">{{ $errors->first('company_logo') }}</p>
                    </div>

                    <div class="form-group{{ $errors->has('company_description') ? ' has-error' : '' }}">
                        <label class="col-lg-12 control-label" for="company_description">Описание компании</label>
                        <textarea class="form-control" id="company_description" name="form[company_description]">{{{
                            $form['company_description'] }}}</textarea>

                        <p class="text-danger">{{ $errors->first('company_description') }}</p>
                    </div>

                    <div class="form-group{{ $errors->has('company_url') ? ' has-error' : '' }}">
                        <label class="col-lg-12 control-label" for="company_url">Сайт компании</label>
                        <input class="form-control" id="company_url" name="form[company_url]" maxlength="100"
                               type="text" value="{{{ $form['company_url'] }}}">

                        <p class="text-danger">{{ $errors->first('company_url') }}</p>
                    </div>
                </div>

            </div>

            <div class="form-group" style="background: #6a7d90;padding: 10px;">
                <button type="button" class="btn btn-default preview">Предпросмотр</button>
                <button type="submit" class="btn btn-primary submit">Сохранить</button>
            </div>

            {{ Form::close() }}

        </div>

    </div>

    <div class="col-md-3 sidebar">


    </div>
</div>
@stop
