@extends('master')

@section('site_title')
{{{ $vacancy->name }}}
@stop

@section('content')
<div class="content col-md-12 well">
    <div class="col-md-9 main">

        @if ( !empty($vacancy->company_id) )
        <ul class="nav nav-pills">
            <li><a href="{{ route('company', $vacancy->company->slug) }}">О компании</a></li>
            <li><a href="{{ route('company_vacancies', $vacancy->company->slug) }}">Вакансии <span class="badge">{{ $vacancy->company->getVacanciesCount() }}</span></a>
            </li>
        </ul>
        @endif

        <div class="vacancy">
            <h2>{{{ $vacancy->name }}}</h2>

            <div class="published">{{{ date("d F Y", strtotime($vacancy->created_at)) }}}</div>
            <hr>

            <div class="meta">
                @if ($vacancy->type == 'fulltime')
                <span class="label label-primary">Фултайм</span>
                @elseif ($vacancy->type == 'contract')
                <span class="label label-success">Контракт</span>
                @else
                <span class="label label-info">Фриланс</span>
                @endif

                @if( !empty($vacancy->location_id) )
                <span class="location"><i class="glyphicon glyphicon-map-marker"></i> {{ $vacancy->getLocationName() }}</span>
                @endif
            </div>

            <div class="block">
                <p>{{ nl2br($vacancy->description) }}</p>
            </div>

            @if( !empty($vacancy->bonuses) )
            <div class="bonuses block">
                <h4>Бонусы</h4>

                <div class="text">{{{ $vacancy->bonuses }}}</div>
            </div>
            @endif

            <div class="block">
                <h4>Необходимые навыки</h4>

                <div class="tags">
                    @foreach($vacancy->getTags() as $tag)
                    <a href="#{{ $tag->slug }}"><span class="label label-danger">{{ $tag->name }}</span></a>
                    @endforeach
                </div>
            </div>

            <div class="reply">
                <div class="instructions block">
                    <h4>Инструкции</h4>

                    <p>{{ nl2br(e($vacancy->instructions)) }}</p>
                </div>

                <a href="" class="replybtn btn btn-primary">Откликнуться на вакансию</a>

                <div class="replyform">
                    <form class="form-horisontal">
                        <div class="form-group">
                            <label class="col-lg-12"><h4>Откликнуться на вакансию</h4></label>
                        </div>

                        <div class="form-group">
                            <label for="inputMessage" class="col-lg-12 control-label">Ваше сообщение<b class="required">*</b></label>

                            <div class="col-lg-12">
                                <textarea class="form-control" style="height: 120px;" id="inputMessage"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail" class="col-lg-12 control-label">Электронная почта для ответа<b
                                    class="required">*</b></label>

                            <div class="col-lg-12">
                                <input type="email" class="form-control" id="inputEmail">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputCode" class="col-lg-12 control-label">Введите код<b class="required">*</b></label>

                            <p class="col-lg-12">
                                <img src="{{ route('captcha', 'vacancy') }}/?{{ str_random() }}" alt=""
                                     onclick="this.src='{{ route('captcha', 'vacancy') }}/?'+Math.random();"
                                     title="{{ Lang::get('user.refresh_code') }}"
                                     style="width: 180px;height: 60px;cursor: pointer;">
                            </p>

                            <div class="col-lg-4">
                                <input type="text" class="form-control" id="inputCode">
                            </div>
                        </div>

                        <div class="form-group col-lg-12">
                            <button type="button" class="replyform-cancel btn btn-default">Отменить</button>
                            <button type="submit" class="btn btn-primary" onclick="alert('Coming soon');return false;">
                                Отправить
                            </button>
                        </div>
                        <div style="clear: both;"></div>
                    </form>
                </div>
            </div>

        </div>
        <div style="clear: both;"></div>
        <div class="share">
            <div class="label">Поделиться ссылкой:</div>
        </div>

    </div>

    <div class="col-md-3 sidebar">

        @if ( !empty($vacancy->company_id) )
        <div class="infoblock">
            <h2><a href="{{ route('company', $vacancy->company->slug) }}">{{{ $vacancy->company->name }}}</a></h2>
            @if( !empty($vacancy->company->logo) )
            <p>
                <a href="{{ route('company', $vacancy->company->slug) }}"><img
                        src="{{ asset('uploads/companies') }}/big/{{ $vacancy->company->logo }}" alt=""></a>
            </p>
            @endif

            <p class="description">
                @if( $vacancy->company->getLocationName() != '' )
                <i class="glyphicon glyphicon-map-marker"></i> {{ $vacancy->company->getLocationName() }}<br>
                @endif
                @if( !empty($vacancy->company->url) )
                <i class="glyphicon glyphicon-link"></i> <a href="{{ $vacancy->company->url }}" target="_blank">{{
                    $vacancy->company->url }}</a><br>
                @endif
            </p>

            @if( !empty($vacancy->company->address) )
            <h3 class="addr">Адреса</h3>

            <p>
                <em>Основной офис</em><br>
                {{{ $vacancy->company->address }}}
            </p>
            @endif
        </div>
        @endif

    </div>
</div>
@stop