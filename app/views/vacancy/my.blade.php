@extends('master')

@section('site_title')
Мои вакансии
@stop

@section('content')
<div class="content col-md-12 well">
    <div class="col-md-9 main">

        <h3>Мои вакансии</h3>

        <ul class="nav nav-pills">
            <li
            {{ $type=='' ? ' class="active"' : '' }}><a href="{{ route('myvacancies') }}">Опублиокванные</a></li>
            <li
            {{ $type=='drafts' ? ' class="active"' : '' }}><a
                href="{{ route('myvacancies') }}?type=drafts">Черновики</a></li>
        </ul>

        <div class="jobslist" style="margin-top: 40px;">
            @foreach ($vacancies as $vacancy)

            {{--
            <a href="{{ route('vacancy', array($i, $i)) }}" class="job featured" target="_blank">
                --}}

                <a href="{{ $vacancy->getUrl() }}" class="job" target="_blank">

                    {{--
                    <div class="favorite active">
                        <i class="glyphicon glyphicon-star" onclick="alert('ok');return false;"></i>
                    </div>

                    <div class="favorite">
                        <i class="glyphicon glyphicon-star-empty" onclick="return false;"></i>
                    </div>
                    --}}

                    <div class="icon">
                        {{ $vacancy->getLogoWidget() }}
                    </div>
                    <div class="title">
                        <h3>{{{ $vacancy->name }}}</h3>
                        <h4>{{{ $vacancy->getCompanyName() }}}</h4>
                    </div>

                    <div class="type">
                        @if ($vacancy->type == 'fulltime')
                        <span class="label label-primary">Фултайм</span>
                        @elseif ($vacancy->type == 'contract')
                        <span class="label label-success">Контракт</span>
                        @else
                        <span class="label label-info">Фриланс</span>
                        @endif

                    </div>

                    @if( !empty($vacancy->location_id) )
                    <div class="location">
                        <i class="glyphicon glyphicon-map-marker"></i> {{ $vacancy->getLocationName() }}
                    </div>
                    @endif
                </a>

                <div class="edit">
                    <a href="{{ route('vacancy_edit', $vacancy->id) }}"
                       class="btn btn-mini btn-warning">Редактировать</a>
                    <a href="#" class="btn btn-mini btn-danger" onclick="alert('Coming soon');return false;">Удалить</a>
                </div>
                @endforeach
        </div>

        {{ $vacancies->links() }}

    </div>

    <div class="col-md-3 sidebar">

        <div class="infoblock">
            @if( !Auth::user()->hasCompany() )
            <a href="{{ route('company_edit') }}" class="btn btn-primary">Создать компанию</a>
            @else
            <div style="text-align: right;">Моя компания</div>
            <h2><a href="{{ route('company', Auth::user()->company->slug) }}">{{{ Auth::user()->company->name }}}</a>
            </h2>
            @if( !empty(Auth::user()->company->logo) )
            <p>
                <a href="{{ route('company', Auth::user()->company->slug) }}"><img
                        src="{{ asset('uploads/companies') }}/big/{{ Auth::user()->company->logo }}" alt=""></a>
            </p>
            @endif

            <p class="description">
                @if( Auth::user()->company->getLocationName() != '' )
                <i class="glyphicon glyphicon-map-marker"></i> {{ Auth::user()->company->getLocationName() }}<br>
                @endif
                @if( !empty(Auth::user()->company->url) )
                <i class="glyphicon glyphicon-link"></i> <a href="{{ Auth::user()->company->url }}" target="_blank">{{
                    Auth::user()->company->url }}</a><br>
                @endif
            </p>

            @if( !empty(Auth::user()->company->address) )
            <h3 class="addr">Адреса</h3>

            <p>
                <em>Основной офис</em><br>
                {{{ Auth::user()->company->address }}}
            </p>
            @endif
            @endif
        </div>

    </div>
</div>
@stop