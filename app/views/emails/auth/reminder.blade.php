<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>{{ Lang::get('user.password_recovery') }}</h2>

<p>
    {{ Lang::get('user.reset_password_instruction', array('home_url'=>URL::route('home'),
    'home_title'=>Lang::get('site.logo'), 'confirmation_url'=>URL::route('resetpassword_confirm', $token))) }}
</p>

<p>
    ---<br>
    {{ Lang::get('site.team') }} <a href="{{ URL::route('home') }}">{{ Lang::get('site.logo') }}</a>.
</p>
</body>
</html>
