<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>{{ Lang::get('user.confirmation_email_subject') }}</h2>

<p>
    {{ Lang::get('site.hello') }}
</p>

<p>
    {{ Lang::get('user.confirmation_message', array('home_url'=>URL::route('home'),
    'home_title'=>Lang::get('site.logo'), 'confirmation_url'=>URL::route('register_confirm', $token))) }}
</p>

<p>{{ Lang::get('user.confirmation_ignore', array('home_url'=>URL::route('home'), 'home_title'=>Lang::get('site.logo')))
    }}</p>

<p>
    ---<br>
    {{ Lang::get('site.team') }} <a href="{{ URL::route('home') }}">{{ Lang::get('site.logo') }}</a>.
</p>
</body>
</html>
