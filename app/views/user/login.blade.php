@extends('master')

@section('site_title')
{{ Lang::get('user.enter') }}
@stop

@section('header')
@stop

@section('footer')
@stop

@section('content')
<div class="wrapper login">
    <div class="header">
        <h1><a href="{{ route('home') }}">{{ Lang::get('site.logo') }}</a></h1>
    </div>

    <div class="content well">
        <div class="main">

            <h3 class="heading">
                <span style="float: left;">{{ Lang::get('user.enter') }}</span>
                <span style="float: right;"><a href="{{ route('register') }}">{{ Lang::get('user.registration')
                        }}</a></span>

                <div style="clear: both;"></div>
            </h3>

            <div class="company_edit">

                @include('flashmessage')

                @if (isset($errors) && count($errors)>0)
                <div class="alert alert-danger">
                    {{ Lang::get('site.errors_happens') }}
                </div>
                @endif

                {{ Form::open(array('url' => route('login'))) }}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label class="col-lg-5 control-label" for="email">Email<b class="required">*</b></label>
                    <input class="form-control" id="email" name="form[email]" maxlength="100" type="email"
                           value="{{{ Input::old('form.email') }}}">

                    <p class="text-danger">{{ $errors->first('email') }}</p>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label class="col-lg-5 control-label" for="password">{{ Lang::get('user.password') }}<b
                            class="required">*</b></label>
                    <input class="form-control" id="password" name="form[password]" maxlength="42" type="password"
                           value="" autocomplete="off">

                    <p class="text-danger">{{ $errors->first('password') }}</p>
                </div>

                <div class="form-group">
                    <div style="float: left;">
                        <button type="submit" class="btn btn-primary">{{ Lang::get('user.enter_action') }}</button>
                    </div>
                    <div style="float: right;padding-top: 8px;">
                        <a href="{{ route('resetpassword') }}">{{ Lang::get('user.remind_password') }}</a>
                    </div>
                    <div style="clear: both;"></div>
                </div>
                {{ Form::close() }}
            </div>

        </div>
    </div>
</div>
@stop