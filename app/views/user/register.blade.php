@extends('master')

@section('site_title')
{{ Lang::get('user.registration') }}
@stop

@section('header')
@stop

@section('footer')
@stop

@section('content')
<div class="wrapper login">
    <div class="header">
        <h1><a href="{{ route('home') }}">{{ Lang::get('site.logo') }}</a></h1>
    </div>

    <div class="content well">
        <div class="main">

            <h3 class="heading">
                <span style="float: left;">{{ Lang::get('user.registration') }}</span>
                <span style="float: right;"><a href="{{ route('login') }}">{{ Lang::get('user.enter') }}</a></span>

                <div style="clear: both;"></div>
            </h3>

            <div class="company_edit">

                @include('flashmessage')

                @if (isset($errors) && count($errors)>0)
                <div class="alert alert-danger">
                    {{ Lang::get('site.errors_happens') }}
                </div>
                @endif

                {{ Form::open(array('url' => route('register'))) }}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label class="col-lg-5 control-label" for="email">{{ Lang::get('user.email') }}<b
                            class="required">*</b></label>
                    <input class="form-control" id="email" name="form[email]" value="{{{ Input::old('form.email') }}}"
                           maxlength="100" type="email">

                    <p class="text-danger">{{ $errors->first('email') }}</p>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label class="col-lg-5 control-label" for="password">{{ Lang::get('user.password') }}<b
                            class="required">*</b></label>
                    <input class="form-control" id="password" name="form[password]" value="" maxlength="42"
                           type="password" autocomplete="off">

                    <p class="text-danger">{{ $errors->first('password') }}</p>
                </div>

                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label class="col-lg-5 control-label" for="password_confirmation">{{
                        Lang::get('user.password_confirmation') }}<b class="required">*</b></label>
                    <input class="form-control" id="password_confirmation" name="form[password_confirmation]" value=""
                           maxlength="42" type="password" autocomplete="off">

                    <p class="text-danger">{{ $errors->first('password_confirmation') }}</p>
                </div>

                <div class="form-group{{ $errors->has('agree') ? ' has-error' : '' }}">
                    <label class="control-label"><input type="checkbox" name="form[agree]" value="1"{{
                        Input::old('form.agree')=='1' ? ' checked="checked"' : ''}}> {{ Lang::get('user.agree_terms',
                        array('link' => route('page', 'terms'), 'terms' => Lang::get('site.agreement'))) }}</label>

                    <p class="text-danger">{{ $errors->first('agree') }}</p>
                </div>

                <div class="form-group">
                    <div style="float: left;">
                        <button type="submit" class="btn btn-primary">{{ Lang::get('user.registration_action') }}
                        </button>
                    </div>
                    <div style="float: right;padding-top: 8px;">
                        <a href="{{ route('resetpassword') }}">{{ Lang::get('user.remind_password') }}</a>
                    </div>
                    <div style="clear: both;"></div>
                </div>
                {{ Form::close() }}
            </div>

        </div>
    </div>
</div>
@stop