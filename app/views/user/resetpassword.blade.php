@extends('master')

@section('site_title')
{{ Lang::get('user.password_recovery') }}
@stop

@section('header')
@stop

@section('footer')
@stop

@section('content')
<div class="wrapper login">
    <div class="header">
        <h1><a href="{{ asset('/') }}">][АНТИМ</a></h1>
    </div>

    <div class="content well">
        <div class="main">

            <h3 class="heading">
                <span style="float: left;">{{ Lang::get('user.password_recovery') }}</span>
                <span style="float: right;"><a href="{{ route('login') }}">{{ Lang::get('user.enter') }}</a></span>

                <div style="clear: both;"></div>
            </h3>

            <div class="company_edit">

                @include('flashmessage')

                @if (isset($errors) && count($errors)>0)
                <div class="alert alert-danger">
                    {{ Lang::get('site.errors_happens') }}
                </div>
                @endif

                {{ Form::open(array('url' => route('resetpassword'))) }}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label class="col-lg-5 control-label" for="email">Email<b class="required">*</b></label>
                    <input class="form-control" id="email" name="email" maxlength="100" type="email"
                           value="{{{ Input::old('email') }}}">

                    <p class="text-danger">{{ $errors->first('email') }}</p>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">{{ Lang::get('site.send') }}</button>
                </div>
                {{ Form::close() }}
            </div>

        </div>
    </div>
</div>
@stop