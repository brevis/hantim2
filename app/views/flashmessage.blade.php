@if ($msg = Session::get('msgInfo'))
<div class="alert alert-info">
    {{ $msg }}
</div>
@endif

@if ($msg = Session::get('msgError'))
<div class="alert alert-danger">
    {{ $msg }}
</div>
@endif

@if ($msg = Session::get('msgSuccess'))
<div class="alert alert-success">
    {{ $msg }}
</div>
@endif

@if ($msg = Session::get('msgWarning'))
<div class="alert alert-warning">
    {{ $msg }}
</div>
@endif