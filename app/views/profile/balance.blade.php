@extends('master')

@section('site_title')
{{ Lang::get('profile.balance') }}
@stop

@section('content')
<div class="content col-md-12 well">
    <div class="col-md-12 main">

        <h3>{{ Lang::get('profile.profile') }}</h3>

        <ul class="nav nav-pills">
            <li><a href="{{ route('profile') }}">{{ Lang::get('site.settings') }}</a></li>
            <li class="active"><a href="{{ route('profile_balance') }}">{{ Lang::get('profile.balance') }}</a></li>
        </ul>

        <div class="company_edit" style="margin-top: 40px;">

            @include('flashmessage')

            @if (isset($errors) && count($errors)>0)
            <div class="alert alert-danger">
                {{ Lang::get('site.errors_happens') }}
            </div>
            @endif

            {{ Form::open(array('url' => route('profile_balance'))) }}
            <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                <label class="col-lg-8" for="code">{{ Lang::get('user.enter_balance_code',
                    array('amount'=>$balanceAmount)) }}:<b class="required">*</b></label>

                <div style="clear: both;"></div>
                <p>
                    <img src="{{ route('captcha', 'balance') }}/?{{ str_random() }}" alt=""
                         onclick="this.src='{{ route('captcha', 'balance') }}/?'+Math.random();"
                         title="{{ Lang::get('user.refresh_code') }}"
                         style="width: 180px;height: 60px;cursor: pointer;">
                </p>
                <input class="form-control" id="code" name="code" value="" maxlength="5" type="text"
                       style="width: 180px;" autocomplete="off">

                <p class="text-danger">{{ $errors->first('code') }}</p>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">{{ Lang::get('site.send') }}</button>
            </div>
            {{ Form::close() }}

        </div>

    </div>
</div>
@stop