@extends('master')

@section('site_title')
{{ Lang::get('profile.balance') }}
@stop

@section('content')
<div class="content col-md-12 well">
    <div class="col-md-12 main">

        <h3>{{ Lang::get('profile.profile') }}</h3>

        <ul class="nav nav-pills">
            <li class="active"><a href="{{ route('profile') }}">{{ Lang::get('site.settings') }}</a></li>
            <li><a href="{{ route('profile_balance') }}">{{ Lang::get('profile.balance') }}</a></li>
        </ul>

        <div class="company_edit" style="margin-top: 40px;">

            @include('flashmessage')

            @if (isset($errors) && count($errors)>0)
            <div class="alert alert-danger">
                {{ Lang::get('site.errors_happens') }}
            </div>
            @endif

            {{ Form::open(array('url' => route('profile'))) }}
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="col-lg-5 control-label" for="email">{{ Lang::get('user.email') }}<b class="required">*</b></label>
                <input class="form-control" id="email" name="form[email]" value="{{{ $form['email'] }}}" maxlength="100"
                       type="email">

                <p class="text-danger">{{ $errors->first('email') }}</p>
            </div>

            <div class="form-group{{ $errors->has('firstName') ? ' has-error' : '' }}">
                <label class="col-lg-5 control-label" for="first_name">{{ Lang::get('user.firstName') }}</label>
                <input class="form-control" id="first_name" name="form[first_name]" value="{{{ $form['first_name'] }}}"
                       maxlength="20" type="text">

                <p class="text-danger">{{ $errors->first('first_name') }}</p>
            </div>

            <div class="form-group{{ $errors->has('lastName') ? ' has-error' : '' }}">
                <label class="col-lg-5 control-label" for="last_name">{{ Lang::get('user.lastName') }}</label>
                <input class="form-control" id="last_name" name="form[last_name]" value="{{{ $form['last_name'] }}}"
                       maxlength="20" type="text">

                <p class="text-danger">{{ $errors->first('last_name') }}</p>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">{{ Lang::get('site.save') }}</button>
            </div>
            {{ Form::close() }}

        </div>

    </div>
</div>
@stop