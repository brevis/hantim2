@extends('master')

@section('site_title')
Избранное
@stop

@section('content')
<div class="content col-md-12 well">
    <div class="col-md-9 main">

        <h3>Избранное</h3>

        <div class="jobslist" style="margin-top: 40px;">
            @for ($i = 0; $i < 10; $i++)
            @if ($i%3==0)
            <a href="{{ asset('/') }}vacancy/123-lorem-ipsum-dolor" class="job featured">
                @else
                <a href="{{ asset('/') }}vacancy/123-lorem-ipsum-dolor" class="job">
                    @endif

                    <div class="favorite active">
                        <i class="glyphicon glyphicon-star" onclick="alert('ok');return false;"></i>
                    </div>

                    <div class="icon">
                        <img src="http://hantim.ru/system/jobs/6/64/64391/job_icon/corel_color_rgb-2.png" alt="">
                    </div>
                    <div class="title">
                        <h3>Веб-разработчик Веб-разработчик Веб-разработчик Веб-разработчик Веб-разработчик</h3>
                        <h4>PRADO</h4>
                    </div>
                    <div class="type">
                        @if ($i%4==0)
                        <span class="label label-primary">Фултайм</span>
                        @elseif ($i%3==0)
                        <span class="label label-success">Контракт</span>
                        @else
                        <span class="label label-info">Фриланс</span>
                        @endif

                    </div>
                    <div class="location">
                        <i class="glyphicon glyphicon-map-marker"></i> Краснодар
                    </div>
                </a>
                @endfor
        </div>

        <ul class="pagination">
            <!--<li><a href="">&lt;&lt;</a></li>-->
            <li class="current"><span>1</span></li>
            <li><a href="">2</a></li>
            <li><a href="">3</a></li>
            <li><span>...</span></li>
            <li><a href="">&gt;&gt;</a></li>
        </ul>

    </div>

    <div class="col-md-3 sidebar">

        <div class="infoblock">
            <h2><a href="{{ asset('/') }}company/omobile">OUMOBILE</a></h2>

            <p>
                <a href="{{ asset('/') }}company/omobile"><img
                        src="http://hantim.ru/system/companies/0/2/2372/logo/logo2.jpg" alt=""></a>
            </p>

            <p class="description">
                <i class="glyphicon glyphicon-map-marker"></i> Краснодар<br>
                <i class="glyphicon glyphicon-link"></i> <a href="">http://www.company.com</a><br>
                Разработка мобильных приложений
            </p>

            <h3 class="addr">Адреса</h3>

            <p>
                <em>Основной офис</em><br>
                Россия, Санкт-Петербург, Стрелка Васильевского острова, Биржевая линия, д.14
            </p>

            <p>
                <em>Основной офис</em><br>
                Россия, Санкт-Петербург, Стрелка Васильевского острова, Биржевая линия, д.14
            </p>
        </div>

    </div>
</div>
@stop