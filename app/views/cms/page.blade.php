@extends('master')

@section('site_title')
    {{ $page->title }}
@stop

@section('content')
<div class="content col-md-12 well">
    <div class="col-md-12 main">

        <h3>{{ $page->title }}</h3>

        <div class="company_edit" style="margin-top: 40px;">
            {{ $page->content }}
        </div>

    </div>
</div>
@stop