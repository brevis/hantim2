<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

	"accepted"             => "Значение :attribute должно быть отмечено.",
	"active_url"           => "Значение :attribute невалидный URL.",
	"after"                => "Значение :attribute должно быть датой после :date.",
	"alpha"                => "Значение :attribute может сожержать только буквы.",
	"alpha_dash"           => "Значение :attribute может сожержать только буквы, цифры и дефис.",
	"alpha_num"            => "Значение :attribute может сожержать только буквы, цифры.",
	"array"                => "Значение :attribute должно быть массивом.",
	"before"               => "Значение :attribute должно быть датой перед :date.",
	"between"              => array(
		"numeric" => "Значение :attribute должно быть между :min и :max.",
		"file"    => "Значение :attribute должно быть между :min и :max Кб.",
		"string"  => "Значение :attribute должно быть между :min and :max символом.",
		"array"   => "Значение :attribute должно быть между :min and :max элементов.",
	),
	"confirmed"            => "Значение :attribute не совпадает с подтверждением.",
	"date"                 => "Значение :attribute невалидная дата.",
	"date_format"          => "Значение :attribute не совпадает с форматом :format.",
	"different"            => "Значения :attribute и :other должны быть разными.",
	"digits"               => "Значение :attribute должно быть :digits цифр.",
	"digits_between"       => "Значение :attribute должно быть между :min and :max цифр.",
	"email"                => "Значение :attribute должно быть валидным email-адресом.",
	"exists"               => "Выбранное значение :attribute невалидно.",
	"image"                => "Значение :attribute должно быть изображением.",
	"in"                   => "Выбранное значение :attribute невалидно.",
	"integer"              => "Значение :attribute должно быть числом.",
	"ip"                   => "Значение :attribute должно быть валидным IP-адресом.",
	"max"                  => array(
		"numeric" => "Значение :attribute не должно превышать :max.",
		"file"    => "Значение :attribute не должнл превышать :max Кб.",
		"string"  => "Значение :attribute не должно превышать :max символов.",
		"array"   => "Значение :attribute не должно превышать :max элементов.",
	),
	"mimes"                => "The :attribute must be a file of type: :values.",
	"min"                  => array(
		"numeric" => "The :attribute must be at least :min.",
		"file"    => "The :attribute must be at least :min kilobytes.",
		"string"  => "Значение :attribute должно быть минимум :min символов.",
		"array"   => "Значение :attribute должно быть минимум :min элементов.",
	),
	"not_in"               => "The selected :attribute is invalid.",
	"numeric"              => "The :attribute must be a number.",
	"regex"                => "The :attribute format is invalid.",
	"required"             => ":attribute обязательное поле.",
	"required_if"          => "The :attribute field is required when :other is :value.",
	"required_with"        => "The :attribute field is required when :values is present.",
	"required_with_all"    => "The :attribute field is required when :values is present.",
	"required_without"     => "The :attribute field is required when :values is not present.",
	"required_without_all" => "The :attribute field is required when none of :values are present.",
	"same"                 => "The :attribute and :other must match.",
	"size"                 => array(
		"numeric" => "The :attribute must be :size.",
		"file"    => "The :attribute must be :size kilobytes.",
		"string"  => "The :attribute must be :size characters.",
		"array"   => "The :attribute must contain :size items.",
	),
	"unique"               => "The :attribute has already been taken.",
	"url"                  => "The :attribute format is invalid.",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => array(
		'attribute-name' => array(
			'rule-name' => 'custom-message',
		),
	),

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => array(
		'password' => 'Пароль'
	),

);
