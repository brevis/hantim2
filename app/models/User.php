<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * Timestamps
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'password_confirmation', 'agree');


    public static $registrationRules = array(
        'email' => 'required|email|unique:users,email',
        'password' => 'required|min:6|max:42|confirmed',
        'password_confirmation' => 'required|min:6|max:42',
        'agree' => 'required',
    );

    public static $loginRules = array(
        'email' => 'required|email',
        'password' => 'required|min:6|max:42',
    );

    public static $resetPasswordRules = array(
        'email' => 'required|email',
    );

    public static $resetPasswordConfirmRules = array(
        'password' => 'required|min:6|max:42|confirmed',
        'password_confirmation' => 'required|min:6|max:42',
    );

    public static $profileRules = array(
        'email' => 'required|email|unique:users,email,',
        'first_name' => 'max:20',
        'last_name' => 'max:20',
    );

    public static function getRegistrationErrorMessages()
    {
        return array(
            'required' => Lang::get('user.required'),
            'unique' => Lang::get('user.email_exists'),
            'email' => Lang::get('user.email_wrong'),
            'confirmed' => Lang::get('user.passwords_different'),
        );
    }

    public static function getLoginErrorMessages()
    {
        return array(
            'required' => Lang::get('user.required'),
            'email' => Lang::get('user.email_wrong'),
        );
    }

    public static function getResetPasswordErrorMessages()
    {
        return array(
            'required' => Lang::get('user.required'),
            'email' => Lang::get('user.email_wrong'),
        );
    }

    public static function getResetPasswordConfirmErrorMessages()
    {
        return array(
            'required' => Lang::get('user.required'),
            'confirmed' => Lang::get('user.passwords_different'),
        );
    }

    public static function getProfileErrorMessages()
    {
        return array(
            'unique' => Lang::get('user.email_exists'),
            'email' => Lang::get('user.email_wrong'),
        );
    }

    /**
     * Relation with Company model
     *
     * @return Company
     */
    public function company()
    {
        return $this->belongsTo('Company', 'id', 'user_id');
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken()
    {
        return $this->remember_token;
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string $value
     * @return void
     */
    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }

    /**
     * Get users balance
     *
     * @return integer
     */
    public function getBalance()
    {
        return intval(DB::table('balance')->where('user_id', $this->id)->pluck('balance'));
    }

    /**
     * Is User has a company
     *
     * @return boolean
     */
    public function hasCompany()
    {
        try {
            $id = intval($this->company->id);
        } catch (\Exception $e) {
            $id = 0;
        }
        return $id > 0;
    }

    /**
     * Return User's company ID if company exists
     */
    public function getCompanyId()
    {
        return is_object($this->company) ? $this->company->id : '';
    }

    /**
     * Decrease User's balance
     *
     * @param $amount
     */
    public function decrementBalance($amount)
    {
        DB::table('balance')->where('user_id', '=', $this->id)->decrement('balance', $amount);
    }

}
