<?php

class Company extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'companies';

    /**
     * Timestamps
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Validation rules for company add/edit form
     *
     * @var integer
     * @return array
     */
    public static function getEditRules($id = 0)
    {
        $rules = array(
            'name' => 'required|max:30',
            'slug' => 'required|min:3|max:20|alpha_dash|unique:companies,slug',
            'address' => 'max:100',
            'url' => 'url|max:100',
            'description_short' => 'max:2000',
            'description_full' => 'max:10000',
            'logo' => 'mimes:jpeg,gif,png|max:524288',
            'employments_count' => 'integer',
            'contact_person' => 'max:20',
            'contact_phone' => 'max:20',
            'created' => 'date',
            'location_id' => 'exists:locations,id',
        );
        if (intval($id) > 0) $rules['slug'] .= ',' . $id;

        return $rules;
    }

    /**
     * Error messages for company add/edit form
     *
     * @return array
     */
    public static function getEditErrorMessages()
    {
        return array(
            'required' => Lang::get('company.required'),
            'unique' => Lang::get('company.slug_exists'),
            'email' => Lang::get('user.email_wrong'),
        );
    }

    /**
     * Relation with Location model
     *
     * @return Location
     */
    public function location()
    {
        return $this->belongsTo('Location', 'location_id');
    }

    /**
     * Relation with User model
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

    /**
     * Assign data to entity fields from add/edit form
     *
     * @param $form
     */
    public function assignFormData($form)
    {
        // TODO: to think how to do it elegantly

        $this->name = $form['name'];
        $this->slug = $form['slug'];
        $this->address = $form['address'];
        $this->url = $form['url'];
        $this->description_short = $form['description_short'];
        $this->description_full = $form['description_full'];
        $this->employments_count = $form['employments_count'];
        $this->contact_person = $form['contact_person'];
        $this->contact_phone = $form['contact_phone'];
        if (!empty($form['location_id'])) {
            $this->location_id = $form['location_id'];
        }
        $this->user_id = $form['user_id'];
        $this->created = '';
        if (!empty($form['year']) && !empty($form['month']) && !empty($form['day'])) {
            $this->created = $form['year'] . '-' . $form['month'] . '-' . $form['day'];
        }
        $this->logo = $form['logo'];
    }

    /**
     * Return form based on Company fields
     *
     * @return array
     */
    public function getForm()
    {
        // TODO: to think how to do it elegantly

        $form = array();

        $form['name'] = $this->name;
        $form['slug'] = $this->slug;
        $form['address'] = $this->address;
        $form['url'] = $this->url;
        $form['description_short'] = $this->description_short;
        $form['description_full'] = $this->description_full;
        $form['employments_count'] = $this->employments_count;
        $form['contact_person'] = $this->contact_person;
        $form['contact_phone'] = $this->contact_phone;
        $form['location_id'] = $this->location_id;
        $form['user_id'] = $this->user_id;
        $form['created'] = $this->created;
        $form['year'] = '';
        $form['month'] = '';
        $form['day'] = '';
        if (!empty($form['created'])) {
            $_ = explode('-', $form['created']);
            $form['year'] = $_[0];
            $form['month'] = $_[1];
            $form['day'] = $_[2];
        }
        $form['logo'] = $this->logo;
        $form['logo_cache'] = $this->logo;
        $form['delete_logo'] = '';

        return $form;
    }

    /**
     * Get vacancies count
     *
     * @return int
     */
    public function getVacanciesCount()
    {
        return DB::table('vacancies')->where('active', 1)->where('company_id', $this->id)->count();
    }

    /**
     * Get location name
     *
     * @return string
     */
    public function getLocationName()
    {
        if (is_object($this->location)) return $this->location->name;
        return '';
    }

    /**
     * Get random companies
     *
     * @param int $count
     * @param bool $withImages
     * @return array
     */
    public static function getRandomCompanies($count = 3, $withImages = true)
    {
        $count = intval($count);
        if ($count < 1) $count = 3;

        if ($withImages) {
            return Company::where('logo', '<>', '')->limit($count)->orderBy(DB::raw('RAND()'))->get();
        } else {
            return Company::limit($count)->orderBy(DB::raw('RAND()'))->get();
        }
    }

    /**
     * Return empty add/edit form
     *
     * @return array
     */
    public static function getEmptyEditForm()
    {
        $form = array();
        foreach (self::getEditRules() as $k => $v) {
            $form[$k] = '';
        }
        $form['day'] = '';
        $form['month'] = '';
        $form['year'] = '';
        $form['logo_cache'] = '';
        $form['delete_logo'] = '';

        return $form;
    }

    /**
     * Get add/edit form from request
     */
    public static function getFormFromRequest()
    {
        $form = self::getEmptyEditForm();
        $reqForm = Input::get('form');
        foreach ($form as $k => $v) {
            if (isset($reqForm[$k])) $form[$k] = $reqForm[$k];
        }

        return $form;
    }

    /**
     * Check date in add/edit form
     *
     * @param $form
     * @return bool
     */
    public static function checkDate($form)
    {
        if (empty($form['day']) && empty($form['month']) && empty($form['year'])) return true;
        return checkdate(intval($form['month']), intval($form['day']), intval($form['year']));
    }

    /**
     * Get upload dir
     *
     * @return string
     */
    public static function getUploadDir()
    {
        return __DIR__ . '/../../public/uploads/companies/';
    }

    /**
     * Save & resize logo
     *
     * @param $key
     * @return string|boolean
     */
    public static function saveLogo($key)
    {
        $file = Input::file($key);

        $filename = str_random(12);
        $dir = substr($filename, 0, 2);
        $ext = $file->getClientOriginalExtension();

        $uploadDir = self::getUploadDir();

        if (!is_dir($uploadDir . 'big/' . $dir)) mkdir($uploadDir . 'big/' . $dir, 0777);
        if (!is_dir($uploadDir . 'medium/' . $dir)) mkdir($uploadDir . 'medium/' . $dir, 0777);
        if (!is_dir($uploadDir . 'small/' . $dir)) mkdir($uploadDir . 'small/' . $dir, 0777);

        Input::file($key)->move($uploadDir . 'big/' . $dir, $filename . '.' . $ext);
        if (!is_file($uploadDir . 'big/' . $dir . '/' . $filename . '.' . $ext)) return false;

        $image = new Simonjarvis\SimpleImage();
        $image->load($uploadDir . 'big/' . $dir . '/' . $filename . '.' . $ext);
        $image->resizeToWidth(190);
        $image->save_with_default_imagetype($uploadDir . 'big/' . $dir . '/' . $filename . '.' . $ext);

        $image = new Simonjarvis\SimpleImage();
        $image->load($uploadDir . 'big/' . $dir . '/' . $filename . '.' . $ext);
        $image->resize(160, 60);
        $image->save_with_default_imagetype($uploadDir . 'medium/' . $dir . '/' . $filename . '.' . $ext);

        $image = new Simonjarvis\SimpleImage();
        $image->load($uploadDir . 'big/' . $dir . '/' . $filename . '.' . $ext);
        $image->resizeToFit(35, 35);
        $image->save_with_default_imagetype($uploadDir . 'small/' . $dir . '/' . $filename . '.' . $ext);

        return $dir . '/' . $filename . '.' . $ext;
    }

    /**
     * Delete logo
     *
     * @param $filename
     */
    public static function deleteLogo($filename)
    {
        $uploadDir = self::getUploadDir();
        if (empty($filename)) return;

        if (is_file($uploadDir . 'big/' . $filename)) @unlink($uploadDir . 'big/' . $filename);
        if (is_file($uploadDir . 'medium/' . $filename)) @unlink($uploadDir . 'medium/' . $filename);
        if (is_file($uploadDir . 'small/' . $filename)) @unlink($uploadDir . 'small/' . $filename);
    }

}
