<?php

class Tag extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tags';

    /**
     * Timestamps
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Return all tags
     *
     * @return array
     */
    public static function getAll()
    {
        return DB::table('tags')->orderBy('name', 'asc')->get();
    }

}
