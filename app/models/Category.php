<?php

class Category extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * Timestamps
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Return all categories
     *
     * @return array
     */
    public static function getAll()
    {
        return DB::table('categories')->orderBy('name', 'asc')->get();
    }

}
