<?php

class Vacancy extends Eloquent
{

    /**
     * How may cost to publish Vacancy
     *
     * @var int
     */
    public static $publishCost = 2;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'vacancies';

    /**
     * Timestamps
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Vacancy "lifetime"
     *
     * @return int
     */
    public static function getLiveTime()
    {
        return 7 * 24 * 3600;
    }

    /**
     * Validation rules for vacancy add/edit form
     *
     * @var $form
     * @return array
     */
    public static function getEditRules($form)
    {
        $rules = array(
            'name' => 'required|max:30',
            'type' => 'required|in:fulltime,contract,freelance',
            'location_id' => 'exists:locations,id',
            'category_id' => 'required|exists:categories,id',
            'description' => 'required|max:10000',
            'bonuses' => 'max:255',
            'contact_person' => 'required|max:20',
            'contact_email' => 'required|max:100|email',
            'instructions' => 'max:2000',
            'logo' => 'mimes:jpeg,gif,png|max:524288',
            'company_id' => 'exists:companies,id',
            'company_name' => 'max:30',
            'company_logo' => 'mimes:jpeg,gif,png|max:524288',
            'company_description' => 'max:2000',
            'company_url' => 'url|max:100',
        );

        if (empty($form['company_id']))
            $rules['company_name'] .= '|required';

        return $rules;
    }

    /**
     * Error messages for company add/edit form
     *
     * @return array
     */
    public static function getEditErrorMessages()
    {
        return array(
            'required' => Lang::get('user.required'),
            'email' => Lang::get('user.email_wrong'),
        );
    }

    /**
     * Relation with Location model
     *
     * @return Location
     */
    public function location()
    {
        return $this->belongsTo('Location', 'location_id');
    }

    /**
     * Relation with Company model
     *
     * @return Company|null
     */
    public function company()
    {
        return $this->belongsTo('Company', 'company_id');
    }

    /**
     * Relation with User model
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

    /**
     * Assign data to entity fields from add/edit form
     *
     * @param $form
     */
    public function assignFormData($form = array())
    {
        // TODO: to think how to do it elegantly

        $this->name = $form['name'];
        $this->type = $form['type'];
        $this->location_id = $form['location_id'];
        $this->user_id = $form['user_id'];
        $this->category_id = $form['category_id'];
        $this->description = $form['description'];
        $this->bonuses = $form['bonuses'];
        $this->contact_person = $form['contact_person'];
        $this->contact_email = $form['contact_email'];
        $this->instructions = $form['instructions'];
        $this->logo = $form['logo'];
        $this->company_id = $form['company_id'];
        $this->company_name = $form['company_name'];
        $this->company_logo = $form['company_logo'];
        $this->company_description = $form['company_description'];
        $this->company_url = $form['company_url'];

        if (empty($this->id)) $this->created_at = $form['created_at'];
    }

    /**
     * Return form based on Company fields
     *
     * @return array
     */
    public function getForm()
    {
        // TODO: to think how to do it elegantly

        $form = array();

        $form['name'] = $this->name;
        $form['type'] = $this->type;
        $form['location_id'] = $this->location_id;
        $form['user_id'] = $this->user_id;
        $form['category_id'] = $this->category_id;
        $form['description'] = $this->description;
        $form['bonuses'] = $this->bonuses;
        $form['contact_person'] = $this->contact_person;
        $form['contact_email'] = $this->contact_email;
        $form['instructions'] = $this->instructions;
        $form['company_id'] = $this->company_id;
        $form['company_name'] = $this->company_name;
        $form['company_logo'] = $this->company_logo;
        $form['company_description'] = $this->company_description;
        $form['company_url'] = $this->company_url;

        $form['logo'] = $this->logo;
        $form['logo_cache'] = $this->logo;
        $form['delete_logo'] = '';

        $form['company_logo'] = $this->comapny_logo;
        $form['company_logo_cache'] = $this->comapny_logo;
        $form['delete_company_logo'] = '';

        $form['tags'] = array();
        foreach (DB::table('tags')
                     ->join('vacancies_tags', 'tags.id', '=', 'vacancies_tags.tag_id')
                     ->where('vacancies_tags.vacancy_id', '=', $this->id)
                     ->select('tags.id')
                     ->get() as $tag) {
            $form['tags'][] = $tag->id;
        }

        $form['active'] = $this->active;

        return $form;
    }

    /**
     * Get location name
     *
     * @return string
     */
    public function getLocationName()
    {
        if (is_object($this->location)) return $this->location->name;
        return '';
    }

    /**
     * Return empty add/edit form
     *
     * @return array
     */
    public static function getEmptyEditForm()
    {
        $form = array();
        foreach (self::getEditRules(array()) as $k => $v) {
            $form[$k] = '';
        }
        $form['logo_cache'] = '';
        $form['delete_logo'] = '';
        $form['company_logo_cache'] = '';
        $form['delete_company_logo'] = '';

        $form['tags'] = array();

        return $form;
    }

    /**
     * Get add/edit form from request
     */
    public static function getFormFromRequest()
    {
        $form = self::getEmptyEditForm();
        $reqForm = Input::get('form');
        foreach ($form as $k => $v) {
            if (isset($reqForm[$k])) $form[$k] = $reqForm[$k];
        }

        return $form;
    }

    /**
     * Get upload dir
     *
     * @return string
     */
    public static function getUploadDir()
    {
        return __DIR__ . '/../../public/uploads/vacancies/';
    }

    /**
     * Save & resize logo
     *
     * @param $key
     * @return string|boolean
     */
    public static function saveLogo($key)
    {
        $file = Input::file($key);

        $filename = str_random(12);
        $dir = substr($filename, 0, 2);
        $ext = $file->getClientOriginalExtension();

        $uploadDir = self::getUploadDir();

        if (!is_dir($uploadDir . $dir)) mkdir($uploadDir . $dir, 0777);

        Input::file($key)->move($uploadDir . $dir, $filename . '.' . $ext);
        if (!is_file($uploadDir . $dir . '/' . $filename . '.' . $ext)) return false;

        $image = new Simonjarvis\SimpleImage();
        $image->load($uploadDir . $dir . '/' . $filename . '.' . $ext);
        $image->resizeToFit(35, 35);
        $image->save_with_default_imagetype($uploadDir . $dir . '/' . $filename . '.' . $ext);

        return $dir . '/' . $filename . '.' . $ext;
    }

    /**
     * Delete logo
     *
     * @param $filename
     */
    public static function deleteLogo($filename)
    {
        $uploadDir = self::getUploadDir();
        if (empty($filename)) return;

        if (is_file($uploadDir . $filename)) @unlink($uploadDir . $filename);
    }

    /**
     * Return vacancy URL
     *
     * @return mixed
     */
    public function getUrl()
    {
        return route('vacancy', array(
            'id' => $this->id,
            'slug' => htmlspecialchars($this->name),
        ));
    }

    /**
     * Return vacancy tags
     *
     * @return array
     */
    public function getTags()
    {
        return DB::table('tags')
            ->join('vacancies_tags', 'tags.id', '=', 'vacancies_tags.tag_id')
            ->where('vacancies_tags.vacancy_id', '=', $this->id)
            ->select('tags.id', 'tags.name', 'tags.slug')
            ->get();
    }

    /**
     * Return vacancy company name
     *
     * @return array
     */
    public function getCompanyName()
    {
        if ($this->company instanceof Company) {
            return $this->company->name;
        } else {
            return $this->company_name;
        }
    }

    /**
     * Return vacancy logo widget
     *
     * @return mixed
     */
    public function getLogoWidget()
    {
        if (!empty($this->logo) && is_file(__DIR__ . '/../../public/uploads/vacancies/' . $this->logo)) {
            return '<img src="' . asset('uploads/vacancies') . '/' . $this->logo . '" alt="">';
        } elseif ($this->company instanceof Company
            && !empty($this->company->logo)
            && is_file(__DIR__ . '/../../public/uploads/companies/small/' . $this->logo)
        ) {
            return '<img src="' . asset('uploads/vacancies/small') . '/' . $this->company->logo . '" alt="">';
        }

        return '&nbsp;';
    }

}
