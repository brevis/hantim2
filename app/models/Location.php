<?php

class Location extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'locations';

    /**
     * Timestamps
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Return all locations
     *
     * @return array
     */
    public static function getAll()
    {
        return DB::table('locations')->orderBy('name', 'asc')->get();
    }

}
