<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Route::get('user/profile', array('as' => 'profile', 'uses' => 'UserController@showProfile'));


// home (search)
Route::any('/', array('as' => 'home', 'uses' => 'SearchController@search'));


// company
Route::any('/company/edit', array('as' => 'company_edit', 'uses' => 'CompanyController@edit'));
Route::any('/company/delete', array('as' => 'company_delete', 'uses' => 'CompanyController@delete'));
Route::any('/company/{slug}', array('as' => 'company', 'uses' => 'CompanyController@show'));
Route::any('/company/{slug}/vacancies', array('as' => 'company_vacancies', 'uses' => 'CompanyController@vacancies'));
Route::any('/companies', array('as' => 'companies', 'uses' => 'CompanyController@showAll'));


// vacancy
Route::any('/vacancy/new', array('as' => 'vacancy_new', 'uses' => 'VacancyController@edit'));
Route::any('/vacancy/{id}/edit', array('as' => 'vacancy_edit', 'uses' => 'VacancyController@edit'));
Route::any('/vacancy/{id}-{slug}', array('as' => 'vacancy', 'uses' => 'VacancyController@show'));
Route::any('/myvacancies', array('as' => 'myvacancies', 'uses' => 'VacancyController@myVacancies'));


// Auth, Registration, Password reset
Route::any('/login', array('as' => 'login', 'uses' => 'UserController@login'));
//Route::any('/login/social', array('as' => 'login_social', 'uses' => 'UserController@loginSocial'));
Route::any('/logout', array('as' => 'logout', 'uses' => 'UserController@logout'));
Route::any('/register', array('as' => 'register', 'uses' => 'UserController@register'));
Route::any('/register/confirm/{code}', array('as' => 'register_confirm', 'uses' => 'UserController@registerConfirm'));
Route::any('/resetpassword', array('as' => 'resetpassword', 'uses' => 'UserController@resetPassword'));
Route::any('/resetpassword/confirm/{code}', array('as' => 'resetpassword_confirm', 'uses' => 'UserController@resetPasswordConfirm'));


// Profile
Route::any('/profile', array('as' => 'profile', 'uses' => 'ProfileController@profile'));
Route::any('/profile/balance', array('as' => 'profile_balance', 'uses' => 'ProfileController@profileBalance'));
Route::any('/favorites', array('as' => 'favorites', 'uses' => 'ProfileController@favorites'));


// cms
Route::any('/{slug}.html', array('as' => 'page', 'uses' => 'CmsController@showPage'));


// captcha
Route::any('/captcha/{key}', array('as' => 'captcha', 'uses' => 'CaptchaController@captcha'));
