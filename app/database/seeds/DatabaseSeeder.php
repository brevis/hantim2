<?php

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        $this->call('PageTableSeeder');
        $this->command->info('Page table seeded!');

        $this->call('LocationTableSeeder');
        $this->command->info('Location table seeded!');

        $this->call('CategoryTableSeeder');
        $this->command->info('Category table seeded!');

        $this->call('TagTableSeeder');
        $this->command->info('Tag table seeded!');
    }

}
