<?php

class CategoryTableSeeder extends Seeder
{

    public function run()
    {
        if (DB::table('categories')->count() > 0) return;

        DB::table('categories')->insert(array(
            array('name' => 'Дизайн', 'description' => 'Дизайн, UI, прототипирование, типографика, аудит', 'slug' => 'design'),
            array('name' => 'Фронтенд', 'description' => 'HTML, CSS, JavaScript, jQuery, Mootools', 'slug' => 'frontend'),
            array('name' => 'Бэкенд', 'description' => 'PHP, Python, RoR, SQL/noSQL, Java, C++', 'slug' => 'backend'),
            array('name' => 'Приложения', 'description' => 'iOS, Android, WP7, Bada, Symbian', 'slug' => 'apps'),
            array('name' => 'Управление и менеджмент', 'description' => 'Управление, менеджмент, консалтинг', 'slug' => 'pm'),
            array('name' => 'Контент', 'description' => 'Журналистика, копирайтинг, рерайтинг, блоггинг', 'slug' => 'content'),
            array('name' => 'Администрирование', 'description' => 'Сети, серверы, базы данных', 'slug' => 'admins'),
            array('name' => 'Тестирование', 'description' => 'Functional, performance, security, stress, load, usability', 'slug' => 'testing'),
            array('name' => 'Разное', 'description' => 'Безопасность, поддержка, аналитика, консультации', 'slug' => 'misc'),
        ));
    }

}