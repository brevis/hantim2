<?php

class TagTableSeeder extends Seeder
{

    public function run()
    {
        if (DB::table('tags')->count() > 0) return;

        DB::table('tags')->insert(array(
            array('name' => 'HTML', 'slug' => 'html'),
            array('name' => 'CSS', 'slug' => 'css'),
            array('name' => 'JavaScript', 'slug' => 'js'),
            array('name' => 'NodeJS', 'slug' => 'nodejs'),
            array('name' => 'PHP', 'slug' => 'php'),
            array('name' => 'Ruby', 'slug' => 'ruby'),
            array('name' => 'Python', 'slug' => 'python'),
            array('name' => 'Java', 'slug' => 'java'),
            array('name' => 'MySQL', 'slug' => 'mysql'),
            array('name' => 'PostgreSQL', 'slug' => 'postgresql'),
            array('name' => 'NoSQL', 'slug' => 'nosql'),
            array('name' => 'MongoDB', 'slug' => 'mongodb'),
            array('name' => 'Highload', 'slug' => 'highload'),
            array('name' => 'OOP', 'slug' => 'oop'),
            array('name' => 'Design Patterns', 'slug' => 'design-patterns'),
            array('name' => 'QA', 'slug' => 'qa'),
            array('name' => 'Laravel', 'slug' => 'laravel'),
            array('name' => 'Symfony', 'slug' => 'symfony'),
            array('name' => 'Ruby on Rail', 'slug' => 'ror'),
            array('name' => 'Django', 'slug' => 'djando'),
        ));
    }

}