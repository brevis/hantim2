<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('pages')) {
            Schema::create('pages', function ($table) {
                $table->increments('id')->unsigned();
                $table->string('slug')->unique();
                $table->string('title')->nullable();
                $table->longText('content')->nullable();
                $table->string('description')->nullable();
                $table->string('keywords')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }

}
