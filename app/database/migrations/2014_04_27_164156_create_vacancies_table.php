<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVacanciesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('vacancies')) {
            Schema::create('vacancies', function ($table) {
                $table->increments('id')->unsigned();
                $table->string('name');
                $table->enum('type', array('fulltime', 'contract', 'freelance'));
                $table->integer('location_id')->nullable()->unsigned();
                $table->integer('category_id')->unsigned();
                $table->text('description');
                $table->string('bonuses')->nullable();
                $table->string('contact_person');
                $table->string('contact_email');
                $table->text('instructions')->nullable();
                $table->string('logo')->nullable();
                $table->integer('company_id')->unsigned()->nullable();
                $table->string('company_name')->nullable();
                $table->string('company_logo')->nullable();
                $table->text('company_description')->nullable();
                $table->string('company_url')->nullable();
                $table->timestamp('created_at');
                $table->integer('user_id')->unsigned();
                $table->boolean('active')->nullable();

                $table->foreign('location_id')->references('id')->on('locations');
                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
                $table->foreign('category_id')->references('id')->on('categories');
                $table->foreign('company_id')->references('id')->on('companies');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacancies');
    }

}
