<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('companies')) {
            Schema::create('companies', function ($table) {
                $table->increments('id')->unsigned();
                $table->string('slug')->unique();
                $table->string('name');
                $table->string('address');
                $table->string('url');
                $table->text('description_short')->nullable();
                $table->longText('description_full')->nullable();
                $table->string('logo')->nullable();
                $table->integer('employments_count');
                $table->string('contact_person');
                $table->string('contact_phone');
                $table->string('created')->nullable();
                $table->integer('location_id')->nullable()->unsigned();
                $table->integer('user_id')->unsigned()->nullable();
                $table->foreign('location_id')->references('id')->on('locations');
                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }

}
