<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('users')) {
            Schema::create('users', function ($table) {
                $table->increments('id')->unsigned();
                $table->string('email')->unique();
                $table->string('password');
                $table->string('first_name')->nullable();
                $table->string('last_name')->nullable();
                $table->boolean('enabled');
                $table->string('remember_token')->nullable();
                $table->string('confirmation_token')->nullable()->index();
                $table->timestamp('created_at')->nullable();
                $table->timestamp('lastLogin_at')->nullable();
                $table->string('last_ip')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }

}
