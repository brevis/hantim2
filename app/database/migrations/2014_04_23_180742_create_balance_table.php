<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBalanceTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('balance')) {
            Schema::create('balance', function ($table) {
                $table->integer('user_id')->unsigned()->unique();
                $table->float('balance')->nullable();
                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            });

            Schema::table('balance', function ($table) {

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('balance');
    }

}
