<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVacanciesTagsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('vacancies_tags')) {
            Schema::create('vacancies_tags', function ($table) {
                $table->integer('tag_id')->unsigned();
                $table->integer('vacancy_id')->unsigned();
                $table->foreign('tag_id')->references('id')->on('tags');
                $table->foreign('vacancy_id')->references('id')->on('vacancies');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacancies_tags');
    }

}
