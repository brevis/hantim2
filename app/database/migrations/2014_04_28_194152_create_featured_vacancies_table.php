<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeaturedVacanciesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('featured_vacancies')) {
            Schema::create('featured_vacancies', function ($table) {
                $table->integer('user_id')->unsigned();
                $table->integer('vacancy_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');
                $table->foreign('vacancy_id')->references('id')->on('vacancies');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('featured_vacancies');
    }

}
